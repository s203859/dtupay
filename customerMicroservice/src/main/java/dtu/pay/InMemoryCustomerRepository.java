package dtu.pay;

import dtu.pay.entities.Customer;
import dtu.pay.valueObjects.*;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Tomas Hagenau Andersen (s233489)
 */
public class InMemoryCustomerRepository implements ICustomerRepository {
    ConcurrentMap<CustomerId, Customer> customers;

    public InMemoryCustomerRepository() {
        customers = new ConcurrentHashMap<>();
    }

    @Override
    public Customer getCustomer(CustomerId customerId) {
        return customers.get(customerId);
    }

    @Override
    public CustomerId createCustomer(BankAccountId bankAccountId) {
        CustomerId customerId = CustomerId.randomId();
        Customer customer = new Customer(bankAccountId, customerId);
        customers.put(customerId, customer);
        return customerId;
    }

    @Override
    public Boolean deleteCustomer(CustomerId customerId) {
        var key = customers.remove(customerId);
        return key != null;
    }
}
