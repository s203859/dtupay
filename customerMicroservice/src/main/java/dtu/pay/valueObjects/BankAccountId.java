package dtu.pay.valueObjects;

import lombok.Value;

@Value
public class BankAccountId {
    String id;
}
