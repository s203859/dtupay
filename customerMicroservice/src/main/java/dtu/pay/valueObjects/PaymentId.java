package dtu.pay.valueObjects;

import lombok.Value;

import java.util.UUID;

@Value
public class PaymentId {
    UUID id;
}
