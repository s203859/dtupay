package dtu.pay.valueObjects;

import dtu.pay.events.CustomerAccountCreated;
import lombok.Value;

import java.util.UUID;

@Value
public class CustomerId {
    UUID id;

    public static CustomerId randomId() {
        return new CustomerId(UUID.randomUUID());
    }
}
