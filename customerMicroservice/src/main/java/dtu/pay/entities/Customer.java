package dtu.pay.entities;

import dtu.pay.valueObjects.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Customer {
    BankAccountId bankAccountId;
    CustomerId customerId;

    public Customer(BankAccountId bankAccountId, CustomerId customerId) {
        this.bankAccountId = bankAccountId;
        this.customerId = customerId;
    }
}
