package dtu.pay.events;

import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.PaymentId;
import lombok.Value;

@Value
public class CustomerIdFound {
    PaymentId paymentId;
    CustomerId customerId;
}
