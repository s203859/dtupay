package dtu.pay.events;

import dtu.pay.valueObjects.CustomerId;
import lombok.Value;

@Value
public class CustomerAccountDeleteRequested {
    CustomerId customerId;
}
