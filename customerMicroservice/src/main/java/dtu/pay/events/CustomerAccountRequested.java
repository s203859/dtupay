package dtu.pay.events;

import dtu.pay.valueObjects.BankAccountId;
import lombok.Value;

@Value
public class CustomerAccountRequested {
    BankAccountId bankAccountId;
}
