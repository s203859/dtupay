package dtu.pay;

import dtu.pay.entities.Customer;
import dtu.pay.events.*;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.EventId;
import messaging.Event;
import messaging.MessageQueue;

/**
 * @author Emil Arnika Skydsgaard (s193579)
 */
public class CustomerService {
    public static final String CUSTOMER_ID_FOUND = "CustomerIdFound";
    public static final String CUSTOMER_BANK_ACCOUNT_ID_FOUND = "CustomerBankAccountIdFound";
    public static final String CUSTOMER_ACCOUNT_REQUESTED = "CustomerAccountRequested";
    public static final String CUSTOMER_ACCOUNT_CREATED = "CustomerAccountCreated";
    public static final String CUSTOMER_ACCOUNT_DELETE_REQUESTED = "CustomerAccountDeleteRequested";
    public static final String CUSTOMER_ACCOUNT_DELETED = "CustomerAccountDeleted";
    public static final String CUSTOMER_ACCOUNT_DELETE_FAILED = "CustomerAccountDeleteFailed";

    MessageQueue messageQueue;
    ICustomerRepository repository;

    public CustomerService(MessageQueue messageQueue, ICustomerRepository repository) {
        this.messageQueue = messageQueue;
        this.repository = repository;

        messageQueue.addHandler(CUSTOMER_ID_FOUND, this::handleCustomerIdFound);
        messageQueue.addHandler(CUSTOMER_ACCOUNT_REQUESTED, this::handleCustomerAccountRequested);
        messageQueue.addHandler(CUSTOMER_ACCOUNT_DELETE_REQUESTED, this::handleCustomerAccountDeleteRequested);
    }



    public void handleCustomerIdFound(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerIdFound received"  + ", eventId: " + eventId.getId().toString());
        CustomerIdFound customerIdFound = event.getArgument(0, CustomerIdFound.class);
        Customer customer = repository.getCustomer(customerIdFound.getCustomerId());

        //send new event
        CustomerBankAccountIdFound customerBankAccountIdFound =
                new CustomerBankAccountIdFound(customerIdFound.getPaymentId(), customer.getBankAccountId());
        System.out.println("CustomerIdFound handler bankAccountId " + customer.getBankAccountId()  + ", eventId: " + eventId.getId().toString());
        Event newEvent = new Event(CUSTOMER_BANK_ACCOUNT_ID_FOUND, new Object[]{customerBankAccountIdFound, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleCustomerAccountRequested(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerAccountRequested received"  + ", eventId: " + eventId.getId().toString());
        CustomerAccountRequested customerAccountRequested = event.getArgument(0, CustomerAccountRequested.class);
        CustomerId customerId = repository.createCustomer(customerAccountRequested.getBankAccountId());

        CustomerAccountCreated customerAccountCreated = new CustomerAccountCreated(customerId);
        Event newEvent = new Event(CUSTOMER_ACCOUNT_CREATED, new Object[]{customerAccountCreated, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleCustomerAccountDeleteRequested(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerAccountDeleteRequested received"  + ", eventId: " + eventId.getId().toString());
        CustomerAccountDeleteRequested customerAccountDeleteRequested = event.getArgument(0, CustomerAccountDeleteRequested.class);
        Boolean success = repository.deleteCustomer(customerAccountDeleteRequested.getCustomerId());

        if (success) {
            CustomerAccountDeleted customerAccountDeleted = new CustomerAccountDeleted();
            Event newEvent = new Event(CUSTOMER_ACCOUNT_DELETED, new Object[]{customerAccountDeleted, eventId});
            messageQueue.publish(newEvent);
        } else {
            String errorMessage = String.format("Customer account with id: %s does not exist", customerAccountDeleteRequested.getCustomerId().getId().toString());
            CustomerAccountDeleteFailed customerAccountDeleteFailed = new CustomerAccountDeleteFailed(errorMessage);
            Event newEvent = new Event(CUSTOMER_ACCOUNT_DELETE_FAILED, new Object[]{customerAccountDeleteFailed, eventId});
            messageQueue.publish(newEvent);
        }
    }

}
