package dtu.pay;

import dtu.pay.entities.Customer;
import dtu.pay.valueObjects.*;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Jonas Løvenhardt Henriksen (s195457)
 */
public interface ICustomerRepository {
    Customer getCustomer(CustomerId customerId);

    CustomerId createCustomer(BankAccountId bankAccountId);

    Boolean deleteCustomer(CustomerId customerId);
}
