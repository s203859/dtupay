package dtu.pay;

import messaging.implementations.RabbitMqQueue;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class StartUp {
    public static void main(String[] args) {
        new StartUp().startUp();
    }

    private void startUp() {
        var mq = new RabbitMqQueue("message_queue");
        var repo = new InMemoryCustomerRepository();
        new CustomerService(mq, repo);
    }
}
