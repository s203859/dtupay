package dtu.pay;

import dtu.pay.entities.Customer;
import dtu.pay.events.*;
import dtu.pay.valueObjects.BankAccountId;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.EventId;
import dtu.pay.valueObjects.PaymentId;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.UUID;

import static org.mockito.Mockito.*;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class CustomerTestSteps {

    private MessageQueue messageQueue;
    private ICustomerRepository mockCustomerRepository, customerRepository;
    private CustomerService customerServiceWithMockRepo, customerService;
    private CustomerId customerId;
    private PaymentId paymentId;
    private BankAccountId bankAccountId;
    private Event event;
    private EventId eventId;

    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        mockCustomerRepository = mock(InMemoryCustomerRepository.class);
        customerRepository = new InMemoryCustomerRepository();
        customerServiceWithMockRepo = new CustomerService(messageQueue, mockCustomerRepository);
        customerService = new CustomerService(messageQueue, customerRepository);
    }

    @Given("an existing customer with customerId {string}")
    public void an_existing_customer_with_customer_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
    }

    @Given("bankAccountId {string}")
    public void bank_account_id(String bankAccountId) {
        this.bankAccountId = new BankAccountId(bankAccountId);
    }

    @When("a {string} event is received")
    public void a_event_is_received(String eventName) {
        this.paymentId = new PaymentId(UUID.randomUUID());
        this.eventId = new EventId(UUID.randomUUID());

        CustomerIdFound customerIdFoundEvent = new CustomerIdFound(this.paymentId, this.customerId);
        Event event = new Event(eventName, new Object[]{customerIdFoundEvent, eventId});

        Customer customer = new Customer(this.bankAccountId, this.customerId);
        when(mockCustomerRepository.getCustomer(this.customerId)).thenReturn(customer);

        customerServiceWithMockRepo.handleCustomerIdFound(event);
    }

    @Then("the {string} event is created with the bankAccountId")
    public void the_event_is_created_with_the_bank_account_id(String eventName) {
        CustomerBankAccountIdFound customerBankAccountIdFound = new CustomerBankAccountIdFound(this.paymentId, this.bankAccountId);
        event = new Event(eventName, new Object[]{customerBankAccountIdFound, eventId});
        Assert.assertEquals(this.bankAccountId, event.getArgument(0, CustomerBankAccountIdFound.class).getBankAccountId());
    }

    @Then("the event is sent")
    public void the_event_is_sent() {
        verify(messageQueue).publish(event);
    }

    @Given("an existing customer")
    public void an_existing_customer() {
        BankAccountId bankAccountId = new BankAccountId("1234567890");
        this.customerId = customerRepository.createCustomer(bankAccountId);
    }

    @When("a CustomerAccountDeleteRequested event is received")
    public void a_customer_account_delete_requested_event_is_received() {
        CustomerAccountDeleteRequested customerAccountDeleteRequested = new CustomerAccountDeleteRequested(this.customerId);
        this.eventId = new EventId(UUID.randomUUID());

        event = new Event(CustomerService.CUSTOMER_ACCOUNT_DELETE_REQUESTED, new Object[]{customerAccountDeleteRequested, eventId});
        customerService.handleCustomerAccountDeleteRequested(event);
    }

    @Then("a CustomerAccountDeleted event is sent")
    public void a_customer_account_deleted_event_is_sent() {
        CustomerAccountDeleted customerAccountDeleted = new CustomerAccountDeleted();
        Event event = new Event(CustomerService.CUSTOMER_ACCOUNT_DELETED, new Object[]{customerAccountDeleted, this.eventId});
        verify(messageQueue).publish(event);
    }

    @Then("the customer does not exist in the repository")
    public void the_customer_does_not_exist_in_the_repository() {
        Assert.assertNull(customerRepository.getCustomer(this.customerId));
    }

    @Given("a CustomerAccountRequested event is received")
    public void a_customer_account_requested_event_is_received() {
        this.bankAccountId = new BankAccountId("1234567890");
        this.eventId = new EventId(UUID.randomUUID());

        this.customerId = new CustomerId(UUID.randomUUID());
        when(mockCustomerRepository.createCustomer(this.bankAccountId)).thenReturn(this.customerId);

        CustomerAccountRequested customerAccountRequested = new CustomerAccountRequested(this.bankAccountId);
        this.event = new Event(CustomerService.CUSTOMER_ACCOUNT_REQUESTED, new Object[]{customerAccountRequested, eventId});
        customerServiceWithMockRepo.handleCustomerAccountRequested(event);
    }

    @Then("a CustomerAccountCreated event is sent")
    public void a_customer_account_created_event_is_sent() {
        CustomerAccountCreated customerAccountCreated = new CustomerAccountCreated(this.customerId);
        this.event = new Event(CustomerService.CUSTOMER_ACCOUNT_CREATED, new Object[]{customerAccountCreated, this.eventId});
        verify(messageQueue).publish(event);
    }

    @Then("the customer gets a customer id")
    public void the_customer_gets_a_customer_id() {
        Assert.assertEquals(this.customerId, event.getArgument(0, CustomerAccountCreated.class).getCustomerId());
    }

    @When("a customer is created")
    public void a_customer_is_created() {
        this.bankAccountId = new BankAccountId("1234567890");
        this.customerId = customerRepository.createCustomer(bankAccountId);
    }

    @Then("a customerId is returned")
    public void a_customer_id_is_returned() {
        Assert.assertNotNull(this.customerId);
    }

    @Then("the customer exists in the repository with the bankAccountId and a customerId")
    public void the_customer_exists_in_the_repository_with_the_bank_account_id_and_a_customer_id() {
        Customer customer = customerRepository.getCustomer(this.customerId);
        Assert.assertEquals(this.bankAccountId, customer.getBankAccountId());
        Assert.assertEquals(this.customerId, customer.getCustomerId());
    }

    @Given("a non existing customer")
    public void a_non_existing_customer() {
    }
    @When("a CustomerAccountDeleteRequested event is received with the invalid customer id {string}")
    public void a_customer_account_delete_requested_event_is_received_with_the_invalid_customer_id(String id) {
        this.customerId = new CustomerId(UUID.fromString(id));
        CustomerAccountDeleteRequested customerAccountDeleteRequested = new CustomerAccountDeleteRequested(this.customerId);
        this.eventId = new EventId(UUID.randomUUID());

        event = new Event(CustomerService.CUSTOMER_ACCOUNT_DELETE_REQUESTED, new Object[]{customerAccountDeleteRequested, eventId});
        customerService.handleCustomerAccountDeleteRequested(event);
    }
    @Then("a CustomerAccountDeleteFailed event is sent with the error message {string}")
    public void a_customer_account_delete_failed_event_is_sent_with_the_error_message(String errorMessage) {
        CustomerAccountDeleteFailed customerAccountDeleteFailed = new CustomerAccountDeleteFailed(errorMessage);
        Event event = new Event(CustomerService.CUSTOMER_ACCOUNT_DELETE_FAILED, new Object[]{customerAccountDeleteFailed, this.eventId});
        verify(messageQueue).publish(event);
    }


}
