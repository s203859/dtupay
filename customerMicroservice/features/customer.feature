# @author Jonas Attrup Rasmussen (s203859)

Feature: Customer

  Scenario: CustomerIdFound event received sends CustomerBankAccountIdFound event
    Given an existing customer with customerId "8e257cf7-15e8-4b45-b7f2-40e84dd3a738"
    And bankAccountId "d4bc245b-0ac9-4a12-a551-888f63fe7244"
    When a "CustomerIdFound" event is received
    Then the "CustomerBankAccountIdFound" event is created with the bankAccountId
    And the event is sent

  Scenario: CustomerAccountRequested event received sends CustomerAccountCreated event
    Given a CustomerAccountRequested event is received
    Then a CustomerAccountCreated event is sent
    And the customer gets a customer id

  Scenario: Fetch newly created customer from repository
    Given bankAccountId "d4bc245b-0ac9-4a12-a551-888f63fe7244"
    When a customer is created
    Then a customerId is returned
    And the customer exists in the repository with the bankAccountId and a customerId

  Scenario: CustomerAccountDeleteRequested event received sends CustomerAccountDeleted event
    Given an existing customer
    When a CustomerAccountDeleteRequested event is received
    Then a CustomerAccountDeleted event is sent
    And the customer does not exist in the repository

  Scenario: CustomerAccountDeleteRequested event received sends CustomerAccountDeleteFailed event
    Given a non existing customer
    When a CustomerAccountDeleteRequested event is received with the invalid customer id "8e257cf7-15e8-4b45-b7f2-40e84dd3a738"
    Then a CustomerAccountDeleteFailed event is sent with the error message "Customer account with id: 8e257cf7-15e8-4b45-b7f2-40e84dd3a738 does not exist"