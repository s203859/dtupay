# DTUPay
Implementation of the final dtupay service in the course "02267 - Software Development of Web Services" by group 2

## Build and Deployment Instructions
We assume a machine running Ubuntu Linux 23.10.

The functionality required to build, test and deploy the DTUPay Service has been contained in a single shell script, in order to easily deploy the entire service. To use this script, the following should be done:
- Install the stated dependencies (JDK21, Maven, Docker CE & docker-compose)
- Add the user to the “docker” group
- Clone the GitLab repository or access the source code from the ZIP file
- Navigate to the root folder of the project (dtupay)
- Run the script `Build_and_deploy.sh`

The shell script `Build_and_deploy.sh` first installs the shared messageUtilities in the environment, then each of the microservices are packaged, and in the process, the service-level tests are run. Once all microservices are packaged, everything is deployed in docker. Finally when every service is up and running, the end-to-end tests are conducted, verifying high-level functionality.

## Software
We assume a machine running Ubuntu Linux 23.10.

### Languages, Build tools & Run-times
- Java 21 - OpenJDK - Version 21.0.1 2023-10-17
  - Installed through apt package manager: `openjdk-21-jdk`
- Maven - Version 3.8.7
  - Installed through apt package manager: `maven`
- Docker CE - version 24.0.5
  - Installed through apt package manager: `docker.io`
- docker-compose - version 1.29.2
  - Installed through apt package manager: `docker-compose`
- Remember to make sure your linux user is added to the docker group `sudo adduser #USER docker`

### Development tools
- IntelliJ IDEA CE - Version 2023.3.2
  - Installation link (snap): https://snapcraft.io/intellij-idea-community
  - Install command: `sudo snap install intellij-idea-community --classic`
