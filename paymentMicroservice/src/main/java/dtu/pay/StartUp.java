package dtu.pay;

import dtu.ws.fastmoney.BankServiceService;
import messaging.implementations.RabbitMqQueue;

/**
 * @author Emil Arnika Skydsgaard (s193579)
 */
public class StartUp {
    public static void main(String[] args) {
        new StartUp().startUp();
    }

    private void startUp() {
        var mq = new RabbitMqQueue("message_queue");
        var repo = new InMemoryPaymentRepository();
        new PaymentService(mq, repo, new BankServiceService().getBankServicePort());
    }
}
