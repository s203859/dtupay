package dtu.pay.entities;

import dtu.pay.valueObjects.*;
import lombok.Data;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class Payment {
    public Payment(TokenId tokenId, MerchantId merchantId, BigDecimal amount) {
        this.paymentId = PaymentId.randomId();
        this.tokenId = tokenId;
        this.merchantId = merchantId;
        this.amount = amount;
    }

    PaymentId paymentId;
    TokenId tokenId;
    CustomerId customerId;
    MerchantId merchantId;
    BankAccountId customerBankAccountId;
    BankAccountId merchantBankAccountId;
    BigDecimal amount;
}
