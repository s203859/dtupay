package dtu.pay;

import dtu.pay.entities.Payment;
import dtu.pay.valueObjects.*;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * @author Jonas Attrup Rasmussen (s203859)
 */
public interface IPaymentRepository {
    PaymentId createPayment(TokenId tokenId, MerchantId merchantId, BigDecimal amount);
    void setCustomerId(PaymentId paymentId, CustomerId customerId);
    void setCustomerBankAccountId(PaymentId paymentId, BankAccountId bankAccountId);
    void setMerchantBankAccountId(PaymentId paymentId, BankAccountId bankAccountId);
    Payment getPayment(PaymentId paymentId);
    Collection<Payment> getPayments();
    Collection<Payment> getPayments(MerchantId merchantId);
    Collection<Payment> getPayments(TokenId[] tokens);
}
