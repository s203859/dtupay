package dtu.pay.events;

import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.TokenId;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class PaymentRequested {
    TokenId tokenId;
    MerchantId merchantId;
    BigDecimal amount;
}
