package dtu.pay.events;

import dtu.pay.valueObjects.PaymentId;
import lombok.Value;

@Value
public class MoneyTransferred {
    PaymentId paymentId;
}
