package dtu.pay.events;

import lombok.Value;

@Value
public class MerchantBankAccountIdFoundFailed {
    String errorMessage;
}
