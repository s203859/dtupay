package dtu.pay.events;

import dtu.pay.valueObjects.BankAccountId;
import dtu.pay.valueObjects.PaymentId;
import lombok.Value;

@Value
public class MerchantBankAccountIdFound {
    PaymentId paymentId;
    BankAccountId bankAccountId;
}
