package dtu.pay.events;

import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;
import dtu.pay.valueObjects.TokenId;
import lombok.Value;

@Value
public class PaymentCreated {
    PaymentId paymentId;
    TokenId tokenId;
    MerchantId merchantId;
}
