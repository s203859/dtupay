package dtu.pay.events;

import lombok.Value;

@Value

public class CustomerIdFoundFailed {
    String errorMessage;
}
