package dtu.pay;

import dtu.pay.entities.Payment;
import dtu.pay.events.*;
import dtu.pay.valueObjects.EventId;
import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;
import dtu.pay.valueObjects.TokenId;
import dtu.ws.fastmoney.BankService;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Collection;

/**
 * @author Jonas Løvenhardt Henriksen (s195457)
 */
public class PaymentService {
    public static final String PAYMENT_REQUESTED = "PaymentRequested";
    public static final String PAYMENT_CREATED = "PaymentCreated";
    public static final String CUSTOMER_ID_FOUND = "CustomerIdFound";
    public static final String MERCHANT_BANK_ACCOUNT_ID_FOUND = "MerchantBankAccountIdFound";
    public static final String MERCHANT_BANK_ACCOUNT_ID_FOUND_FAILED = "MerchantBankAccountIdFoundFailed";
    public static final String CUSTOMER_BANK_ACCOUNT_ID_FOUND = "CustomerBankAccountIdFound";
    public static final String MONEY_TRANSFERRED = "MoneyTransferred";
    public static final String MANAGER_REPORT_REQUESTED = "ManagerReportRequested";
    public static final String MANAGER_REPORT_GENERATED = "ManagerReportGenerated";
    public static final String MERCHANT_REPORT_REQUESTED = "MerchantReportRequested";
    public static final String MERCHANT_REPORT_GENERATED = "MerchantReportGenerated";
    public static final String CUSTOMER_USED_TOKENS_FOUND = "CustomerUsedTokensFound";
    public static final String CUSTOMER_REPORT_GENERATED = "CustomerReportGenerated";
    public static final String MONEY_TRANSFER_FAILED = "MoneyTransferFailed";
    public static final String CUSTOMER_ID_FOUND_FAILED = "CustomerIdFoundFailed";
    MessageQueue messageQueue;
    IPaymentRepository repository;
    BankService bank;

    public PaymentService(MessageQueue messageQueue, IPaymentRepository repository, BankService bank) {
        this.messageQueue = messageQueue;
        this.repository = repository;
        this.bank = bank;

        messageQueue.addHandler(PAYMENT_REQUESTED, this::handlePaymentRequested);
        messageQueue.addHandler(CUSTOMER_ID_FOUND, this::handleCustomerIdFound);
        messageQueue.addHandler(CUSTOMER_ID_FOUND_FAILED, this::handleCustomerIdFoundFailed);
        messageQueue.addHandler(MERCHANT_BANK_ACCOUNT_ID_FOUND_FAILED, this::handleMerchantBankAccountIdFoundFailed);
        messageQueue.addHandler(MERCHANT_BANK_ACCOUNT_ID_FOUND, this::handleMerchantBankAccountIdFound);
        messageQueue.addHandler(CUSTOMER_BANK_ACCOUNT_ID_FOUND, this::handleCustomerBankAccountIdFound);
        messageQueue.addHandler(MANAGER_REPORT_REQUESTED, this::handleManagerReportRequested);
        messageQueue.addHandler(MERCHANT_REPORT_REQUESTED, this::handleMerchantReportRequested);
        messageQueue.addHandler(CUSTOMER_USED_TOKENS_FOUND, this::handleCustomerUsedTokensFound);
    }

    public void handlePaymentRequested(Event event) {
        PaymentRequested paymentRequested = event.getArgument(0, PaymentRequested.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerUsedTokensRequested received" + ", eventId: " + eventId.getId().toString());
        PaymentId paymentId = repository.createPayment(paymentRequested.getTokenId(), paymentRequested.getMerchantId(), paymentRequested.getAmount());

        //send new event
        PaymentCreated paymentCreated = new PaymentCreated(paymentId, paymentRequested.getTokenId(), paymentRequested.getMerchantId());
        Event newEvent = new Event(PAYMENT_CREATED, new Object[]{paymentCreated, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleCustomerIdFound(Event event) {
        CustomerIdFound customerIdFound = event.getArgument(0, CustomerIdFound.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerIdFound received" + ", eventId: " + eventId.getId().toString());
        repository.setCustomerId(customerIdFound.getPaymentId(), customerIdFound.getCustomerId());
    }

    public void handleCustomerIdFoundFailed(Event event) {
        CustomerIdFoundFailed customerIdFoundFailed =
                event.getArgument(0, CustomerIdFoundFailed.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerIdFoundFailed received" + ", eventId: " + eventId.getId().toString());
        MoneyTransferFailed moneyTransferFailed = new MoneyTransferFailed(customerIdFoundFailed.getErrorMessage());
        Event newEvent = new Event(MONEY_TRANSFER_FAILED, new Object[]{moneyTransferFailed, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleMerchantBankAccountIdFound(Event event) {
        MerchantBankAccountIdFound merchantBankAccountIdFound = event.getArgument(0, MerchantBankAccountIdFound.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("MerchantBankAccountIdFound received" + ", eventId: " + eventId.getId().toString());
        System.out.println("MerchantBankAccountId in handle" + merchantBankAccountIdFound.getBankAccountId() + ", eventId: " + eventId.getId().toString());

        repository.setMerchantBankAccountId(merchantBankAccountIdFound.getPaymentId(), merchantBankAccountIdFound.getBankAccountId());

        Payment payment = repository.getPayment(merchantBankAccountIdFound.getPaymentId());
        transferMoney(eventId, payment);
    }

    public void handleMerchantBankAccountIdFoundFailed(Event event) {
        MerchantBankAccountIdFoundFailed merchantBankAccountIdFoundFailed = event.getArgument(0, MerchantBankAccountIdFoundFailed.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("MerchantBankAccountIdFoundFailed received" + ", eventId: " + eventId.getId().toString());
        MoneyTransferFailed moneyTransferFailed = new MoneyTransferFailed(merchantBankAccountIdFoundFailed.getErrorMessage());
        Event newEvent = new Event(MONEY_TRANSFER_FAILED, new Object[]{moneyTransferFailed, eventId});
        messageQueue.publish(newEvent);
    }


    public void handleCustomerBankAccountIdFound(Event event) {
        CustomerBankAccountIdFound customerBankAccountIdFound = event.getArgument(0, CustomerBankAccountIdFound.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerBankAccountIdFound received" + ", eventId: " + eventId.getId().toString());
        System.out.println("CustomerBankAccountId in handle " + customerBankAccountIdFound.getBankAccountId() + ", eventId: " + eventId.getId().toString());
        repository.setCustomerBankAccountId(customerBankAccountIdFound.getPaymentId(), customerBankAccountIdFound.getBankAccountId());

        Payment payment = repository.getPayment(customerBankAccountIdFound.getPaymentId());
        transferMoney(eventId, payment);
    }

    public void transferMoney(EventId eventId, Payment payment) {
        if (eventId != null) {
            System.out.println("transferMoney" + ", eventId: " + eventId.getId().toString());
            System.out.println("CustomerBankAccountId " + payment.getCustomerBankAccountId() + ", eventId: " + eventId.getId().toString());
            System.out.println("MerchantBankAccountId " + payment.getMerchantBankAccountId() + ", eventId: " + eventId.getId().toString());
        }
        if (payment.getCustomerBankAccountId() != null && payment.getMerchantBankAccountId() != null) {
            try {
                System.out.println("READY TO TRANSFER MONEY");
                bank.transferMoneyFromTo(payment.getCustomerBankAccountId().getId(), payment.getMerchantBankAccountId()
                        .getId(), payment.getAmount(), payment.getPaymentId().getId().toString());

                //send new event
                MoneyTransferred moneyTransferred = new MoneyTransferred(payment.getPaymentId());
                Event newEvent = new Event(MONEY_TRANSFERRED, new Object[]{moneyTransferred, eventId});
                messageQueue.publish(newEvent);
            } catch (Exception e) {
                MoneyTransferFailed moneyTransferFailed = new MoneyTransferFailed(e.getMessage());
                Event newEvent = new Event(MONEY_TRANSFER_FAILED, new Object[]{moneyTransferFailed, eventId});
                messageQueue.publish(newEvent);
            }
        }
    }

    public void handleManagerReportRequested(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("ManagerReportRequested received" + ", eventId: " + eventId.getId().toString());
        Collection<Payment> payments = repository.getPayments();
        ManagerReportGenerated managerReportGenerated = new ManagerReportGenerated(payments);
        Event newEvent = new Event(MANAGER_REPORT_GENERATED, new Object[]{managerReportGenerated, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleMerchantReportRequested(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("MerchantReportRequested received" + ", eventId: " + eventId.getId().toString());
        MerchantId merchantId = event.getArgument(0, MerchantReportRequested.class).getMerchantId();
        Collection<Payment> payments = repository.getPayments(merchantId);
        MerchantReportGenerated merchantReportGenerated = new MerchantReportGenerated(payments);
        Event newEvent = new Event(MERCHANT_REPORT_GENERATED, new Object[]{merchantReportGenerated, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleCustomerUsedTokensFound(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerUsedTokensFound received" + ", eventId: " + eventId.getId().toString());
        TokenId[] tokens = event.getArgument(0, CustomerUsedTokensFound.class).getTokenIds();
        Collection<Payment> payments = repository.getPayments(tokens);
        CustomerReportGenerated customerReportGenerated = new CustomerReportGenerated(payments);
        Event newEvent = new Event(CUSTOMER_REPORT_GENERATED, new Object[]{customerReportGenerated, eventId});
        messageQueue.publish(newEvent);
    }
}
