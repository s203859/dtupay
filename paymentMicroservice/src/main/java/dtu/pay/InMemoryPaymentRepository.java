package dtu.pay;

import dtu.pay.entities.Payment;
import dtu.pay.valueObjects.*;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class InMemoryPaymentRepository implements IPaymentRepository {
    ConcurrentMap<PaymentId, Payment> payments;

    public InMemoryPaymentRepository() {
        this.payments = new ConcurrentHashMap<>();
    }

    @Override
    public PaymentId createPayment(TokenId tokenId, MerchantId merchantId, BigDecimal amount) {
        Payment payment = new Payment(tokenId, merchantId, amount);
        payments.put(payment.getPaymentId(), payment);
        return payment.getPaymentId();
    }

    @Override
    public void setCustomerId(PaymentId paymentId, CustomerId customerId) {
        // TODO: error handling;
        payments.get(paymentId).setCustomerId(customerId);
    }

    @Override
    public void setCustomerBankAccountId(PaymentId paymentId, BankAccountId bankAccountId) {
        // TODO: error handling;
        payments.get(paymentId).setCustomerBankAccountId(bankAccountId);
    }

    @Override
    public void setMerchantBankAccountId(PaymentId paymentId, BankAccountId bankAccountId) {
        // TODO: error handling;
        payments.get(paymentId).setMerchantBankAccountId(bankAccountId);
    }

    @Override
    public Payment getPayment(PaymentId paymentId) {
        // TODO: error handling;
        return payments.get(paymentId);
    }

    @Override
    public Collection<Payment> getPayments() {
        return payments.values();
    }

    @Override
    public Collection<Payment> getPayments(MerchantId merchantId) {
        return payments.values().stream().filter(x->x.getMerchantId().equals(merchantId)).toList();
    }

    @Override
    public Collection<Payment> getPayments(TokenId[] tokens) {
        return payments.values().stream().filter(x-> Arrays.stream(tokens).toList().contains(x.getTokenId())).toList();
    }
}
