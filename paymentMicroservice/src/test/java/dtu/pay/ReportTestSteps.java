package dtu.pay;

import dtu.pay.entities.Payment;
import dtu.pay.events.*;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.EventId;
import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.TokenId;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class ReportTestSteps {
    ManagerReportRequested managerReportRequested;
    EventId eventId;
    PaymentService paymentService;
    private MessageQueue messageQueue;
    private IPaymentRepository paymentRepository;
    private BankService bank;
    Collection<Payment> payments;
    Event event;
    private MerchantId merchantId;
    private MerchantReportRequested merchantReportRequested;
    private CustomerUsedTokensFound customerUsedTokensFound;
    private TokenId tokenId1;
    private TokenId tokenId2;

    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        paymentRepository = new InMemoryPaymentRepository();
        bank = new BankServiceService().getBankServicePort();
        paymentService = new PaymentService(messageQueue, paymentRepository, bank);
    }

    //region Manager
    @Given("a ManagerReportRequested event")
    public void a_manager_report_requested_event() {
        managerReportRequested = new ManagerReportRequested();
    }

    @Given("two existing payments")
    public void two_existing_payments() {
        Payment payment1 = new Payment(new TokenId(UUID.randomUUID()), new MerchantId(UUID.randomUUID()),
                BigDecimal.valueOf(100));
        Payment payment2 = new Payment(new TokenId(UUID.randomUUID()), new MerchantId(UUID.randomUUID()),
                BigDecimal.valueOf(200));
        payments = new ArrayList<>();
        payments.add(payment1);
        payments.add(payment2);
        payment1.setPaymentId(paymentRepository.createPayment(payment1.getTokenId(), payment1.getMerchantId(), payment1.getAmount()));
        payment2.setPaymentId(paymentRepository.createPayment(payment2.getTokenId(), payment2.getMerchantId(), payment2.getAmount()));
    }

    @When("the ManagerReportRequested event is received")
    public void the_manager_report_requested_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        Event event = new Event(PaymentService.MANAGER_REPORT_REQUESTED, new Object[]{managerReportRequested, eventId});
        paymentService.handleManagerReportRequested(event);
    }

    @Then("a ManagerReportGenerated event is sent")
    public void a_manager_report_generated_event_is_sent() {
        ManagerReportGenerated managerReportGenerated = new ManagerReportGenerated(paymentRepository.getPayments());
        event = new Event(PaymentService.MANAGER_REPORT_GENERATED, new Object[]{managerReportGenerated, eventId});
        verify(messageQueue).publish(event);
    }

    @Then("the event contains all payments")
    public void the_event_contains_all_payments() {
        Collection<Payment> paymentsReceived = event.getArgument(0, ManagerReportGenerated.class).getPayments();
        Assert.assertEquals(payments.size(), paymentsReceived.size());
        Assert.assertTrue(paymentsReceived.containsAll(payments));
    }
    //endregion

    //region Merchant
    @Given("a MerchantReportRequested event with MerchantId {string}")
    public void a_merchant_report_requested_event_with_merchant_id(String merchantId) {
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
        merchantReportRequested = new MerchantReportRequested(this.merchantId);
    }
    @Given("two existing payments where one has the same MerchantId")
    public void two_existing_payments_where_one_has_the_same_merchant_id() {
        Payment payment1 = new Payment(new TokenId(UUID.randomUUID()), merchantId,
                BigDecimal.valueOf(100));
        Payment payment2 = new Payment(new TokenId(UUID.randomUUID()), new MerchantId(UUID.randomUUID()),
                BigDecimal.valueOf(200));
        payments = new ArrayList<>();
        payments.add(payment1);
        payments.add(payment2);
        payment1.setPaymentId(paymentRepository.createPayment(payment1.getTokenId(), payment1.getMerchantId(), payment1.getAmount()));
        payment2.setPaymentId(paymentRepository.createPayment(payment2.getTokenId(), payment2.getMerchantId(), payment2.getAmount()));
    }
    @When("the MerchantReportRequested event is received")
    public void the_merchant_report_requested_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        Event event = new Event(PaymentService.MERCHANT_REPORT_REQUESTED, new Object[]{merchantReportRequested, eventId});
        paymentService.handleMerchantReportRequested(event);
    }
    @Then("a MerchantReportGenerated event is sent")
    public void a_merchant_report_generated_event_is_sent() {
        MerchantReportGenerated merchantReportGenerated =
                new MerchantReportGenerated(paymentRepository.getPayments(merchantId));
        event = new Event(PaymentService.MERCHANT_REPORT_GENERATED, new Object[]{merchantReportGenerated, eventId});
        verify(messageQueue).publish(event);
    }
    @Then("the event contains only the payment with the merchantId")
    public void the_event_contains_only_the_payment_with_the_merchant_id() {
        Collection<Payment> paymentsReceived = event.getArgument(0, MerchantReportGenerated.class).getPayments();
        List<Payment> merchantPayments = payments.stream().filter(x -> x.getMerchantId().equals(merchantId)).toList();
        Assert.assertEquals(merchantPayments.size()
                , paymentsReceived.size());
        Assert.assertTrue(paymentsReceived.containsAll(merchantPayments));
    }
    //endregion Merchant

    //region Customer
    @Given("a CustomerUsedTokensFound event with tokenIds {string} and {string}")
    public void a_customer_used_tokens_found_event_with_token_ids_and(String tokenId1, String tokenId2) {
        this.tokenId1 = new TokenId(UUID.fromString(tokenId1));
        this.tokenId2 = new TokenId(UUID.fromString(tokenId2));
        customerUsedTokensFound = new CustomerUsedTokensFound(new TokenId[]{this.tokenId1, this.tokenId2});
    }
    @Given("three existing payments where two have the same tokenIds")
    public void three_existing_payments_where_two_have_the_same_token_ids() {
        Payment payment1 = new Payment(this.tokenId1, new MerchantId(UUID.randomUUID()),
                BigDecimal.valueOf(100));
        Payment payment2 = new Payment(this.tokenId2, new MerchantId(UUID.randomUUID()),
                BigDecimal.valueOf(200));
        Payment payment3 = new Payment(new TokenId(UUID.randomUUID()), new MerchantId(UUID.randomUUID()),
                BigDecimal.valueOf(200));
        payments = new ArrayList<>();
        payments.add(payment1);
        payments.add(payment2);
        payments.add(payment3);
        payment1.setPaymentId(paymentRepository.createPayment(payment1.getTokenId(), payment1.getMerchantId(), payment1.getAmount()));
        payment2.setPaymentId(paymentRepository.createPayment(payment2.getTokenId(), payment2.getMerchantId(), payment2.getAmount()));
        payment3.setPaymentId(paymentRepository.createPayment(payment3.getTokenId(), payment3.getMerchantId(), payment3.getAmount()));
    }
    @When("the CustomerUsedTokensFound event is received")
    public void the_customer_used_tokens_found_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        Event event = new Event(PaymentService.CUSTOMER_USED_TOKENS_FOUND, new Object[]{customerUsedTokensFound, eventId});
        paymentService.handleCustomerUsedTokensFound(event);
    }
    @Then("a CustomerReportGenerated event is sent")
    public void a_customer_report_generated_event_is_sent() {
        CustomerReportGenerated customerReportGenerated =
                new CustomerReportGenerated(paymentRepository.getPayments(new TokenId[]{tokenId1, tokenId2}));
        event = new Event(PaymentService.CUSTOMER_REPORT_GENERATED, new Object[]{customerReportGenerated, eventId});
        verify(messageQueue).publish(event);
    }
    @Then("the event contains only the payments with the tokenIds")
    public void the_event_contains_only_the_payments_with_the_token_ids() {
        Collection<Payment> paymentsReceived = event.getArgument(0, CustomerReportGenerated.class).getPayments();
        List<Payment> customerPayments = payments.stream().filter(x -> x.getTokenId().equals(tokenId1) || x.getTokenId()
                .equals(tokenId2)).toList();
        Assert.assertEquals(customerPayments.size(), paymentsReceived.size());
        Assert.assertTrue(paymentsReceived.containsAll(customerPayments));
    }
    //endregion

}
