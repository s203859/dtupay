package dtu.pay;

import dtu.pay.events.*;
import dtu.pay.valueObjects.*;
import dtu.pay.entities.Payment;
import dtu.ws.fastmoney.*;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;

import static dtu.pay.PaymentService.CUSTOMER_ID_FOUND_FAILED;
import static dtu.pay.PaymentService.MONEY_TRANSFER_FAILED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class PaymentTestSteps {
    private PaymentRequested paymentRequested;
    private TokenId tokenId;
    private MerchantId merchantId
            ;
    private BigDecimal amount;
    private PaymentService paymentService;
    private PaymentService paymentServiceWithMockBank;
    private MessageQueue messageQueue;
    private IPaymentRepository paymentRepository;
    private EventId eventId;
    private PaymentId paymentId;
    private BankService bank;
    private BankService bankMock;
    private String customerAccountId;
    private String merchantAccountId;
    private Exception exception;
    private Event event;

    @Before
    public void before() throws BankServiceException_Exception {
        bankMock = mock(BankService.class);
        messageQueue = mock(MessageQueue.class);
        paymentRepository = new InMemoryPaymentRepository();
        bank = new BankServiceService().getBankServicePort();
        paymentService = new PaymentService(messageQueue, paymentRepository, bank);
        paymentServiceWithMockBank = new PaymentService(messageQueue, paymentRepository, bankMock);

        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        customerAccountId = bank.createAccountWithBalance(customer, BigDecimal.valueOf(1000));

        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        merchantAccountId = bank.createAccountWithBalance(merchant, BigDecimal.valueOf(2000));
    }

    @After
    public void clearAccounts() throws Exception {

        if (customerAccountId != null) {
            bank.retireAccount(customerAccountId);
            customerAccountId = "";
        }

        if (merchantAccountId != null) {
            bank.retireAccount(merchantAccountId);
            merchantAccountId = "";
        }
    }

    @Given("a paymentRequest event with tokenId {string} merchantId {string} and amount {int}")
    public void a_payment_request_event_with_token_id_merchant_id_and_amount(String tokenId, String merchantId, Integer amount) {
        this.tokenId = new TokenId(UUID.fromString(tokenId));
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
        this.amount = new BigDecimal(amount);
        paymentRequested = new PaymentRequested(this.tokenId, this.merchantId, this.amount);
    }
    @When("the PaymentRequested event is received")
    public void the_payment_requested_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        Event event = new Event(PaymentService.PAYMENT_REQUESTED, new Object[]{paymentRequested, eventId});
        paymentService.handlePaymentRequested(event);
    }
    @Then("a payment is created with a nonempty paymentId")
    public void a_payment_is_created_with_a_nonempty_payment_id() {
        Collection<Payment> payments = paymentRepository.getPayments();
        Payment payment = payments.stream().findFirst().get();
        paymentId = payment.getPaymentId();
        Assert.assertNotNull(paymentId);
    }
    @Then("the payment has tokenId {string} merchantId {string} and amount {int}")
    public void the_payment_has_token_id_merchant_id_and_amount(String tokenId, String merchantId, int amount) {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertEquals(new TokenId(UUID.fromString(tokenId)), payment.getTokenId());
        Assert.assertEquals(new MerchantId(UUID.fromString(merchantId)), payment.getMerchantId());
        Assert.assertEquals(BigDecimal.valueOf(amount), payment.getAmount());
    }
    @Then("a PaymentCreated event is sent")
    public void a_payment_created_event_is_sent() {
        PaymentCreated paymentCreated = new PaymentCreated(paymentId, tokenId, merchantId);
        Event event = new Event(PaymentService.PAYMENT_CREATED, new Object[]{paymentCreated, eventId});
        verify(messageQueue).publish(event);
    }

    @Given("a payment in the repository with a paymentId")
    public void a_payment_in_the_repository_with_a_payment_id() {
        paymentId = paymentRepository.createPayment(new TokenId(UUID.randomUUID()), new MerchantId(UUID.randomUUID()), new BigDecimal(100));
    }
    @When("the CustomerIdFound event is received")
    public void the_customer_id_found_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        CustomerIdFound customerIdFound = new CustomerIdFound(paymentId, new CustomerId(UUID.randomUUID()));
        Event event = new Event(PaymentService.CUSTOMER_ID_FOUND, new Object[]{customerIdFound, eventId});
        paymentService.handleCustomerIdFound(event);
    }
    @Then("the customerId of the payment is set")
    public void the_customer_id_of_the_payment_is_set() {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertNotNull(payment.getCustomerId());
    }
    @Given("the CustomerBankAccountId is null")
    public void the_customer_bank_account_id_is_null() {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertNull(payment.getCustomerBankAccountId());
    }
    @When("the MerchantBankAccountIdFound event is received")
    public void the_merchant_bank_account_id_found_event_is_received() throws BankServiceException_Exception {
        eventId = new EventId(UUID.randomUUID());
        MerchantBankAccountIdFound merchantBankAccountIdFound = new MerchantBankAccountIdFound(paymentId, new BankAccountId(merchantAccountId));
        Event event = new Event(PaymentService.MERCHANT_BANK_ACCOUNT_ID_FOUND, new Object[]{merchantBankAccountIdFound, eventId});
        paymentService.handleMerchantBankAccountIdFound(event);
    }
    @Then("the MerchantBankAccountId of the payment is set")
    public void the_merchant_bank_account_id_of_the_payment_is_set() {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertEquals(new BankAccountId(merchantAccountId), payment.getMerchantBankAccountId());
    }
    @Given("the MerchantBankAccountId is null")
    public void the_merchant_bank_account_id_is_null() {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertNull(payment.getMerchantBankAccountId());
    }
    @Given("the customerId of the payment is already set")
    public void the_customer_id_of_the_payment_is_already_set() {
        paymentRepository.getPayment(paymentId).setCustomerId(new CustomerId(UUID.randomUUID()));
    }
    @When("the CustomerBankAccountIdFound event is received")
    public void the_customer_bank_account_id_found_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        CustomerBankAccountIdFound customerBankAccountIdFound = new CustomerBankAccountIdFound(paymentId, new BankAccountId(customerAccountId));
        Event event = new Event(PaymentService.CUSTOMER_BANK_ACCOUNT_ID_FOUND, new Object[]{customerBankAccountIdFound, eventId});
        paymentService.handleCustomerBankAccountIdFound(event);
    }
    @Then("the CustomerBankAccountId of the payment is set")
    public void the_customer_bank_account_id_of_the_payment_is_set() {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertEquals(new BankAccountId(customerAccountId), payment.getCustomerBankAccountId());
    }
    @Given("the CustomerBankAccountId is set")
    public void the_customer_bank_account_id_is_set() {
        paymentRepository.getPayment(paymentId).setCustomerBankAccountId(new BankAccountId(customerAccountId));
    }
    @Then("the money is transferred")
    public void the_money_is_transferred() throws BankServiceException_Exception {
        Assert.assertEquals(BigDecimal.valueOf(900), bank.getAccount(customerAccountId).getBalance());
        Assert.assertEquals(BigDecimal.valueOf(2100), bank.getAccount(merchantAccountId).getBalance());
    }
    @Then("a MoneyTransferred event is sent")
    public void a_money_transferred_event_is_sent() {
        MoneyTransferred moneyTransferred = new MoneyTransferred(paymentId);
        Event event = new Event(PaymentService.MONEY_TRANSFERRED, new Object[]{moneyTransferred, eventId});
        verify(messageQueue).publish(event);
    }

    @Given("the MerchantBankAccountId is set")
    public void the_merchant_bank_account_id_is_set() {
        paymentRepository.getPayment(paymentId).setMerchantBankAccountId(new BankAccountId(merchantAccountId));
    }
    @Given("the merchantId of the payment is already set")
    public void the_merchant_id_of_the_payment_is_already_set() {
        Payment payment = paymentRepository.getPayment(paymentId);
        Assert.assertEquals(new BankAccountId(merchantAccountId), payment.getMerchantBankAccountId());
    }
    @When("trying to transfer the money")
    public void trying_to_transfer_the_money() throws BankServiceException_Exception {
        Payment payment = paymentRepository.getPayment(paymentId);
        exception = new Exception("MoneyTransferFailed");

        doAnswer(invocationOnMock -> {
            throw exception;
        }).when(bankMock).transferMoneyFromTo(
                payment.getCustomerBankAccountId().getId(),
                payment.getMerchantBankAccountId().getId(),
                payment.getAmount(),
                payment.getPaymentId().getId().toString());

        paymentServiceWithMockBank.transferMoney(eventId, payment);
    }
    @Then("a MoneyTransferFailed event is sent")
    public void a_money_transfer_failed_event_is_sent() {
        MoneyTransferFailed moneyTransferFailed = new MoneyTransferFailed(exception.getMessage());
        event = new Event(MONEY_TRANSFER_FAILED, new Object[]{ moneyTransferFailed, eventId });
        verify(messageQueue).publish(event);
    }

    @Then("with an exception")
    public void with_an_exception() {
        MoneyTransferFailed e = event.getArgument(0, MoneyTransferFailed.class);
        Assert.assertEquals(exception.getMessage(), e.getErrorMessage());
    }
    @When("the CustomerIdFoundFailed event is received")
    public void the_customer_id_found_failed_event_is_received() {
        exception = new Exception("Customer token does not exist or have been used");
        eventId = new EventId(UUID.randomUUID());
        CustomerIdFoundFailed customerIdFoundFailed =
                new CustomerIdFoundFailed("Customer token does not exist or have been used");
        Event event = new Event(CUSTOMER_ID_FOUND_FAILED, new Object[]{ customerIdFoundFailed, eventId});
        paymentService.handleCustomerIdFoundFailed(event);
    }

    @When("the MerchantBankAccountIdFoundFailed event is received")
    public void the_merchant_bank_account_id_found_failed_event_is_received() {
        exception = new Exception("Merchant not found");
        eventId = new EventId(UUID.randomUUID());
        MerchantBankAccountIdFoundFailed merchantBankAccountIdFoundFailed =
                new MerchantBankAccountIdFoundFailed("Merchant not found");
        Event event = new Event(CUSTOMER_ID_FOUND_FAILED, new Object[]{ merchantBankAccountIdFoundFailed, eventId});
        paymentService.handleMerchantBankAccountIdFoundFailed(event);
    }
}
