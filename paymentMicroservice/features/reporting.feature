# @author Martin Posselt Munck (s213791)

Feature: Reporting

  Scenario: ManagerReportRequested event received and ManagerReportGenerated sent
    Given a ManagerReportRequested event
    And two existing payments
    When the ManagerReportRequested event is received
    Then a ManagerReportGenerated event is sent
    And the event contains all payments

  Scenario: MerchantReportRequested event received and MerchantReportGenerated sent
    Given a MerchantReportRequested event with MerchantId "63301b3c-c31c-4186-b5d9-5701924f03e2"
    And two existing payments where one has the same MerchantId
    When the MerchantReportRequested event is received
    Then a MerchantReportGenerated event is sent
    And the event contains only the payment with the merchantId

  Scenario: CustomerUsedTokensFound event received and CustomerReportGenerated sent
    Given a CustomerUsedTokensFound event with tokenIds "8ec0543d-3178-4def-9929-1950f10a1b8e" and "65987ecf-e17f-4049-92ec-9a322502326a"
    And three existing payments where two have the same tokenIds
    When the CustomerUsedTokensFound event is received
    Then a CustomerReportGenerated event is sent
    And the event contains only the payments with the tokenIds