# @author Tomas Hagenau Andersen (s233489)

Feature: Payment

  Scenario: Payment requested
    Given a paymentRequest event with tokenId "3fcc7ed1-dc4a-4e66-b78e-8029d7c71754" merchantId "8e257cf7-15e8-4b45-b7f2-40e84dd3a738" and amount 100
    When the PaymentRequested event is received
    Then a payment is created with a nonempty paymentId
    And the payment has tokenId "3fcc7ed1-dc4a-4e66-b78e-8029d7c71754" merchantId "8e257cf7-15e8-4b45-b7f2-40e84dd3a738" and amount 100
    And a PaymentCreated event is sent

  Scenario: Customer Id found
    Given a payment in the repository with a paymentId
    When the CustomerIdFound event is received
    Then the customerId of the payment is set

  Scenario: Customer not found
    Given a paymentRequest event with tokenId "3fcc7ed1-dc4a-4e66-b78e-8029d7c71754" merchantId "8e257cf7-15e8-4b45-b7f2-40e84dd3a738" and amount 100
    When the CustomerIdFoundFailed event is received
    Then a MoneyTransferFailed event is sent
    And with an exception

  Scenario: Merchant bank account not found
    Given a paymentRequest event with tokenId "3fcc7ed1-dc4a-4e66-b78e-8029d7c71754" merchantId "8e257cf7-15e8-4b45-b7f2-40e84dd3a738" and amount 100
    When the MerchantBankAccountIdFoundFailed event is received
    Then a MoneyTransferFailed event is sent
    And with an exception

  Scenario: MerchantBankAccountIdFound event and CustomerBankAccountId is not set
    Given a payment in the repository with a paymentId
    And the CustomerBankAccountId is null
    When the MerchantBankAccountIdFound event is received
    Then the MerchantBankAccountId of the payment is set

  Scenario: CustomerBankAccountIdFound event and MerchantBankAccountId is not set
    Given a payment in the repository with a paymentId
    And the MerchantBankAccountId is null
    And the customerId of the payment is already set
    When the CustomerBankAccountIdFound event is received
    Then the CustomerBankAccountId of the payment is set

  Scenario: MerchantBankAccountIdFound event and CustomerBankAccountId is set
    Given a payment in the repository with a paymentId
    And the CustomerBankAccountId is set
    And the customerId of the payment is already set
    When the MerchantBankAccountIdFound event is received
    Then the MerchantBankAccountId of the payment is set
    And the money is transferred
    And a MoneyTransferred event is sent

  Scenario: CustomerBankAccountIdFound event and MerchantBankAccountId is set
    Given a payment in the repository with a paymentId
    And the MerchantBankAccountId is set
    And the merchantId of the payment is already set
    When the CustomerBankAccountIdFound event is received
    Then the CustomerBankAccountId of the payment is set
    And the money is transferred
    And a MoneyTransferred event is sent

  Scenario: MerchantBankAccountIdFound and CustomerBankAccountId are set
    Given a payment in the repository with a paymentId
    And the CustomerBankAccountId is set
    And the MerchantBankAccountId is set
    When trying to transfer the money
    Then a MoneyTransferFailed event is sent
    And with an exception