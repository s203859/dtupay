package dtu.pay;

import dtu.pay.entities.Payment;
import dtu.pay.events.*;
import dtu.pay.exceptions.CustomerReportException;
import dtu.pay.valueObjects.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static dtu.pay.DTUPayService.*;
import static org.mockito.Mockito.*;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class ReportTestSteps {

    private MessageQueue messageQueue;
    private DTUPayService dtuPayService;
    private ManagerReportRequested managerReportRequested;
    private MerchantReportRequested merchantReportRequested;
    private CustomerReportRequested customerReportRequested;
    private MerchantId merchantId;
    private CustomerId customerId;
    private EventId eventId;
    private Collection<Payment> payments;
    private Collection<Payment> paymentsReturned;
    private boolean success;
    private Exception exception;
    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        dtuPayService = new DTUPayService(messageQueue);
    }

    //region Manager
    @Given("ManagerReportRequested")
    public void manager_report_requested() {
        managerReportRequested = new ManagerReportRequested();
    }
    @When("creating the manager report")
    public void creating_the_manager_report() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            this.payments = List.of(new Payment[]{
                    new Payment(
                            new PaymentId(UUID.randomUUID()),
                            new TokenId(UUID.randomUUID()),
                            new CustomerId(UUID.randomUUID()),
                            new MerchantId(UUID.randomUUID()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BigDecimal(100)
                    ),
                    new Payment(
                            new PaymentId(UUID.randomUUID()),
                            new TokenId(UUID.randomUUID()),
                            new CustomerId(UUID.randomUUID()),
                            new MerchantId(UUID.randomUUID()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BigDecimal(200)
                    )
            });
            var payload = new ManagerReportGenerated(this.payments);
            var newEvent = new Event(MANAGER_REPORT_GENERATED, new Object[] { payload, eventId});
            dtuPayService.handleManagerReportGenerated(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        this.paymentsReturned = dtuPayService.generateManagerReport();
    }
    @Then("all payments are returned")
    public void all_payments_are_returned() {
        Assert.assertEquals(this.payments, this.paymentsReturned);
    }

    @Then("a ManagerReportRequested event is sent")
    public void a_manager_report_requested_event_is_sent() {
        var event = new Event(MANAGER_REPORT_REQUESTED, new Object[] { managerReportRequested, eventId });
        verify(messageQueue).publish(event);
    }
    //endregion

    //region Merchant
    @Given("a merchant with merchantId {string}")
    public void a_merchant_with_merchant_id(String merchantId) {
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
    }
    @Given("MerchantReportRequested")
    public void merchant_report_requested() {
        merchantReportRequested = new MerchantReportRequested(this.merchantId);
    }
    @When("creating the merchant report")
    public void creating_the_merchant_report() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            this.payments = List.of(new Payment[]{
                    new Payment(
                            new PaymentId(UUID.randomUUID()),
                            new TokenId(UUID.randomUUID()),
                            new CustomerId(UUID.randomUUID()),
                            this.merchantId,
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BigDecimal(100)
                    ),
                    new Payment(
                            new PaymentId(UUID.randomUUID()),
                            new TokenId(UUID.randomUUID()),
                            new CustomerId(UUID.randomUUID()),
                            this.merchantId,
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BigDecimal(200)
                    )
            });
            var payload = new MerchantReportGenerated(this.payments);
            var newEvent = new Event(MERCHANT_REPORT_GENERATED, new Object[] { payload, eventId});
            dtuPayService.handleMerchantReportGenerated(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        this.paymentsReturned = dtuPayService.generateMerchantReport(merchantReportRequested);
    }
    @Then("all payments with the merchantId are returned")
    public void all_payments_with_the_merchant_id_are_returned() {
        Assert.assertEquals(this.payments, this.paymentsReturned);
    }
    @Then("a MerchantReportRequested event is sent")
    public void a_merchant_report_requested_event_is_sent() {
        var event = new Event(MERCHANT_REPORT_REQUESTED, new Object[] { merchantReportRequested, eventId });
        verify(messageQueue).publish(event);
    }
    //endregion

    //region Customer
    @Given("a customer with customerId {string}")
    public void a_customer_with_customer_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
    }
    @Given("CustomerReportRequested")
    public void customer_report_requested() {
        this.customerReportRequested = new CustomerReportRequested(this.customerId);
    }
    @When("creating the customer report")
    public void creating_the_customer_report() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            this.payments = List.of(new Payment[]{
                    new Payment(
                            new PaymentId(UUID.randomUUID()),
                            new TokenId(UUID.randomUUID()),
                            this.customerId,
                            new MerchantId(UUID.randomUUID()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BigDecimal(100)
                    ),
                    new Payment(
                            new PaymentId(UUID.randomUUID()),
                            new TokenId(UUID.randomUUID()),
                            this.customerId,
                            new MerchantId(UUID.randomUUID()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BankAccountId(UUID.randomUUID().toString()),
                            new BigDecimal(200)
                    )
            });
            var payload = new CustomerReportGenerated(this.payments);
            var newEvent = new Event(CUSTOMER_REPORT_GENERATED, new Object[] { payload, eventId});
            dtuPayService.handleCustomerReportGenerated(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try {
            this.paymentsReturned = dtuPayService.generateCustomerReport(customerReportRequested);
            this.success = true;
        } catch (CustomerReportException e) {
            this.success = false;
            this.exception = e;
        }
    }
    @Then("all payments with the tokens from the customer Id are returned")
    public void all_payments_with_the_tokens_from_the_customer_id_are_returned() {
        Assert.assertEquals(this.payments, this.paymentsReturned);
    }
    @Then("a CustomerUsedTokensRequested event is sent")
    public void a_customer_used_tokens_requested_event_is_sent() {
        var request = new CustomerUsedTokensRequested(this.customerId);
        var event = new Event(CUSTOMER_USED_TOKENS_REQUESTED, new Object[] { request, eventId });
        verify(messageQueue).publish(event);
    }
    @When("creating the customer report for customer without used tokens")
    public void creating_the_customer_report_for_customer_without_used_tokens() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            var payload = new CustomerUsedTokensFoundFailed(
                    String.format("Customer with id %s does not have any used tokens",
                            this.customerId.getId().toString()));
            var newEvent = new Event(CUSTOMER_USED_TOKENS_FOUND_FAILED, new Object[] { payload, eventId});
            dtuPayService.handleCustomerUsedTokensFoundFailed(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try {
            this.paymentsReturned = dtuPayService.generateCustomerReport(customerReportRequested);
            this.success = true;
        } catch (CustomerReportException e) {
            this.success = false;
            this.exception = e;
        }
    }
    @Then("it is not successful")
    public void it_is_not_successful() {
        Assert.assertFalse(success);
    }
    @Then("an exception with error message {string}\"")
    public void an_exception_with_error_message(String errorMessage) {
        Assert.assertEquals(errorMessage, exception.getMessage());
    }
    //endregion
}
