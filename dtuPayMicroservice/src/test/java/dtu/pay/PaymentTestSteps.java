package dtu.pay;

import dtu.pay.events.CustomerAccountCreated;
import dtu.pay.events.MoneyTransferFailed;
import dtu.pay.events.MoneyTransferred;
import dtu.pay.events.PaymentRequested;
import dtu.pay.exceptions.PaymentException;
import dtu.pay.valueObjects.*;
import io.cucumber.java.Before;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static dtu.pay.DTUPayService.*;
import static org.mockito.Mockito.*;

/**
 * @author Tomas Hagenau Andersen (s233489)
 */
public class PaymentTestSteps {

    private MessageQueue messageQueue;
    private MessageQueue messageQueueMock;
    private DTUPayService dtuPayService;
    private DTUPayService dtuPayServiceMock;
    private PaymentRequested paymentRequested;
    private BigDecimal amount;
    private TokenId tokenId;
    private MerchantId merchantId;
    private PaymentId paymentId;
    private String errorMessage;
    private EventId eventId;
    private volatile Event publishedEvent;
    private final CompletableFuture<PaymentId> requestResult = new CompletableFuture<>();
    private boolean success;


    @Before
    public void before() {
        messageQueueMock = mock(MessageQueue.class);
        dtuPayServiceMock = new DTUPayService(messageQueueMock);
        messageQueue = new MessageQueue() {
            @Override
            public void publish(Event event) {
                publishedEvent = event;
            }

            @Override
            public void addHandler(String eventType, Consumer<Event> handler) {
            }

        };
        dtuPayService = new DTUPayService(messageQueue);
    }

    @Given("a payment request for amount {int}")
    public void a_payment_request_for_amount(int amount) {
        this.amount = new BigDecimal(amount);
    }

    @Given("tokenId {string}")
    public void token_id(String tokenId) {
        this.tokenId = new TokenId(UUID.fromString(tokenId));
    }

    @Given("merchantId {string}")
    public void merchant_id(String merchantId) {
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
    }

    @When("a payment is requested")
    public void a_payment_is_requested() {
        paymentRequested = new PaymentRequested(tokenId, merchantId, amount);
        new Thread(() -> {
            try {
                requestResult.complete(dtuPayService.requestPayment(paymentRequested));
            } catch (PaymentException e) {
                throw new RuntimeException(e);
            }
        }).start();
    }

    @Then("a PaymentRequested event is sent")
    public void a_payment_requested_event_is_sent() {
        // wait until event has been sent
        while (publishedEvent == null) Thread.onSpinWait();
        Assert.assertNotNull(publishedEvent);
        Assert.assertEquals(paymentRequested, publishedEvent.getArgument(0, PaymentRequested.class));
        Assert.assertNotNull(publishedEvent.getArgument(1, EventId.class));
        Assert.assertEquals(DTUPayService.PAYMENT_REQUESTED, publishedEvent.getType());
    }

    @Given("the payment has been requested")
    public void the_payment_has_been_requested() {
        paymentRequested = new PaymentRequested(tokenId, merchantId, amount);
        new Thread(() -> {
            try {
                requestResult.complete(dtuPayService.requestPayment(paymentRequested));
            } catch (PaymentException e) {
                throw new RuntimeException(e);
            }
        }).start();
    }

    @When("a MoneyTransferred event is received")
    public void a_money_transferred_event_is_received() {
        // Retrieve eventId from PaymentRequested event after it has been sent
        while (publishedEvent == null) Thread.onSpinWait();
        EventId eventId = publishedEvent.getArgument(1, EventId.class);

        // construct and send moneyTransferred event
        paymentId = new PaymentId(UUID.randomUUID());
        var moneyTransferred = new MoneyTransferred(paymentId);
        Event event = new Event(DTUPayService.MONEY_TRANSFERRED, new Object[]{moneyTransferred, eventId});
        dtuPayService.handleMoneyTransferred(event);
    }

    @Then("the paymentId of the payment is returned to the caller")
    public void the_payment_id_of_the_payment_is_returned_to_the_caller() {
        Assert.assertEquals(paymentId, requestResult.join());
    }

    @Given("PaymentRequested")
    public void payment_requested() {
        paymentRequested = new PaymentRequested(
                new TokenId(UUID.randomUUID()),
                new MerchantId(UUID.randomUUID()),
                new BigDecimal(100));
    }
    @When("creating the payment")
    public void creating_the_payment() throws PaymentException {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            var paymentId = new PaymentId(UUID.randomUUID());
            var moneyTransferred = new MoneyTransferred(paymentId);
            Event newEvent = new Event(MONEY_TRANSFERRED, new Object[] { moneyTransferred, eventId});
            dtuPayServiceMock.handleMoneyTransferred(newEvent);
            return null;
        }).when(messageQueueMock).publish(any(Event.class));

        paymentId = dtuPayServiceMock.requestPayment(paymentRequested);
    }
    @Then("the payment id is returned")
    public void the_payment_id_is_returned() {
        Assert.assertNotNull(paymentId);
    }
    @Then("a MoneyTransferred event is sent")
    public void a_money_transferred_event_is_sent() {
        Event event = new Event(PAYMENT_REQUESTED, new Object[] { paymentRequested, eventId });
        verify(messageQueueMock).publish(event);
    }
    @When("creating the invalid payment")
    public void creating_the_invalid_payment() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            String errorMessage = "MoneyTransferFailed";
            var moneyTransferFailed = new MoneyTransferFailed(errorMessage);
            Event newEvent = new Event(MONEY_TRANSFER_FAILED, new Object[] { moneyTransferFailed, eventId});
            dtuPayServiceMock.handleMoneyTransferFailed(newEvent);
            return null;
        }).when(messageQueueMock).publish(any(Event.class));

        try {
            paymentId = dtuPayServiceMock.requestPayment(paymentRequested);
            success = true;
        } catch (PaymentException e) {
            success = false;
            errorMessage = e.getMessage();
        }
    }
    @Then("the money transfer is not successful")
    public void the_money_transfer_is_not_successful() {
        Assert.assertFalse(success);
    }
    @Then("it has an error message")
    public void it_has_an_error_message() {
        Assert.assertNotNull(errorMessage);
    }
}
