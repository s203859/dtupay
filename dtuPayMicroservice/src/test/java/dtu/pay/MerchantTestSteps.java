package dtu.pay;

import dtu.pay.events.*;
import dtu.pay.exceptions.DeleteMerchantException;
import dtu.pay.valueObjects.BankAccountId;
import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.EventId;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.UUID;

import static dtu.pay.DTUPayService.*;
import static org.mockito.Mockito.*;

/**
 * @author Jonas Løvenhardt Henriksen (s195457)
 */
public class MerchantTestSteps {

    private MessageQueue messageQueue;
    private DTUPayService dtuPayService;
    private MerchantAccountRequested merchantAccountRequested;
    private MerchantAccountDeleteRequested merchantAccountDeleteRequested;
    private MerchantId merchantId;
    private EventId eventId;
    private boolean success;
    private Exception exception;
    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        dtuPayService = new DTUPayService(messageQueue);
    }

    @Given("a MerchantAccountRequested with bank id {string}")
    public void a_merchant_account_requested_with_bank_id(String bankId) {
        BankAccountId bankAccountId = new BankAccountId(bankId);
        merchantAccountRequested = new MerchantAccountRequested(bankAccountId);
    }
    @When("creating a new merchant")
    public void creating_a_new_merchant() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            MerchantId merchantId = new MerchantId(UUID.randomUUID());
            MerchantAccountCreated merchantAccountCreated = new MerchantAccountCreated(merchantId);
            Event newEvent = new Event(MERCHANT_ACCOUNT_CREATED, new Object[] { merchantAccountCreated, eventId});
            dtuPayService.handleMerchantAccountCreated(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        merchantId = dtuPayService.createMerchant(merchantAccountRequested);
    }
    @Then("a new merchant id is returned")
    public void a_new_merchant_id_is_returned() {
        Assert.assertNotNull(merchantId);
    }

    @Then("a MerchantAccountRequested is sent")
    public void a_merchant_account_requested_is_sent() {
        Event event = new Event(MERCHANT_ACCOUNT_REQUESTED, new Object[] {merchantAccountRequested, eventId });
        verify(messageQueue).publish(event);
    }

    @Given("a MerchantAccountDeleteRequested with merchant id {string}")
    public void a_merchant_account_delete_requested_with_merchant_id(String merchantId) {
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
        merchantAccountDeleteRequested = new MerchantAccountDeleteRequested(this.merchantId);
    }
    @When("deleting an existing merchant")
    public void deleting_an_existing_merchant() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            MerchantAccountDeleted merchantAccountDeleted = new MerchantAccountDeleted();
            Event newEvent = new Event(MERCHANT_ACCOUNT_DELETED, new Object[] { merchantAccountDeleted, eventId});
            dtuPayService.handleMerchantAccountDeleted(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try{
            dtuPayService.deleteMerchant(merchantAccountDeleteRequested);
            success = true;
        } catch (DeleteMerchantException e) {
            success = false;
            exception = e;
        }
    }
    @Then("the merchant is deleted")
    public void the_merchant_is_deleted() {
        Assert.assertTrue(success);
        Assert.assertNull(exception);
    }

    @When("deleting a non existing merchant")
    public void deleting_a_non_existing_merchant() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            MerchantAccountDeleteFailed merchantAccountDeleteFailed = new MerchantAccountDeleteFailed("");
            Event newEvent = new Event(MERCHANT_ACCOUNT_DELETE_FAILED, new Object[] { merchantAccountDeleteFailed, eventId});
            dtuPayService.handleMerchantAccountDeleteFailed(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try{
            dtuPayService.deleteMerchant(merchantAccountDeleteRequested);
            success = true;
        } catch (DeleteMerchantException e) {
            success = false;
            exception = e;
        }
    }
    @Then("the merchant deletion failed")
    public void the_merchant_deletion_failed() {
        Assert.assertFalse(success);
        Assert.assertNotNull(exception);
    }
}
