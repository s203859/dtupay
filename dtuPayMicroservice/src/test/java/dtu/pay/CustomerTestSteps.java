package dtu.pay;

import dtu.pay.events.*;
import dtu.pay.exceptions.DeleteCustomerException;
import dtu.pay.valueObjects.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.UUID;

import static dtu.pay.DTUPayService.*;
import static org.mockito.Mockito.*;

/**
 * @author Emil Arnika Skydsgaard (s193579)
 */
public class CustomerTestSteps {

    private MessageQueue messageQueue;
    private DTUPayService dtuPayService;
    private CustomerAccountRequested customerAccountRequested;
    private CustomerAccountDeleteRequested customerAccountDeleteRequested;
    private CustomerId customerId;
    private EventId eventId;
    private boolean success;
    private Exception exception;
    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        dtuPayService = new DTUPayService(messageQueue);
    }

    @Given("a CustomerAccountRequested with bank id {string}")
    public void a_customer_account_requested_with_bank_id(String bankId) {
        BankAccountId bankAccountId = new BankAccountId(bankId);
        customerAccountRequested = new CustomerAccountRequested(bankAccountId);
    }
    @When("creating a new customer")
    public void creating_a_new_customer() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            CustomerId customerId = new CustomerId(UUID.randomUUID());
            CustomerAccountCreated customerAccountCreated = new CustomerAccountCreated(customerId);
            Event newEvent = new Event(CUSTOMER_ACCOUNT_CREATED, new Object[] { customerAccountCreated, eventId});
            dtuPayService.handleCustomerAccountCreated(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        customerId = dtuPayService.createCustomer(customerAccountRequested);
    }
    @Then("a new customer id is returned")
    public void a_new_customer_id_is_returned() {
        Assert.assertNotNull(customerId);
    }

    @Then("a CustomerAccountRequested is sent")
    public void a_customer_account_requested_is_sent() {
        Event event = new Event(CUSTOMER_ACCOUNT_REQUESTED, new Object[] {customerAccountRequested, eventId });
        verify(messageQueue).publish(event);
    }

    @Given("a CustomerAccountDeleteRequested with customer id {string}")
    public void a_customer_account_delete_requested_with_customer_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
        customerAccountDeleteRequested = new CustomerAccountDeleteRequested(this.customerId);
    }
    @When("deleting an existing customer")
    public void deleting_an_existing_customer() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            CustomerAccountDeleted customerAccountDeleted = new CustomerAccountDeleted();
            Event newEvent = new Event(CUSTOMER_ACCOUNT_DELETED, new Object[] { customerAccountDeleted, eventId});
            dtuPayService.handleCustomerAccountDeleted(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try{
            dtuPayService.deleteCustomer(customerAccountDeleteRequested);
            success = true;
        } catch (DeleteCustomerException e) {
            success = false;
            exception = e;
        }
    }
    @Then("the customer is deleted")
    public void the_customer_is_deleted() {
        Assert.assertTrue(success);
        Assert.assertNull(exception);
    }

    @When("deleting a non existing customer")
    public void deleting_a_non_existing_customer() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            CustomerAccountDeleteFailed customerAccountDeleteFailed = new CustomerAccountDeleteFailed("");
            Event newEvent = new Event(CUSTOMER_ACCOUNT_DELETE_FAILED, new Object[] { customerAccountDeleteFailed, eventId});
            dtuPayService.handleCustomerAccountDeleteFailed(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try{
            dtuPayService.deleteCustomer(customerAccountDeleteRequested);
            success = true;
        } catch (DeleteCustomerException e) {
            success = false;
            exception = e;
        }
    }
    @Then("the customer deletion failed")
    public void the_customer_deletion_failed() {
        Assert.assertFalse(success);
        Assert.assertNotNull(exception);
    }
}
