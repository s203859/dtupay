package dtu.pay;

import dtu.pay.events.*;
import dtu.pay.exceptions.TokenGenerationException;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.EventId;
import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.TokenId;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.Collection;
import java.util.UUID;

import static dtu.pay.DTUPayService.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class TokenTestSteps {
    private MessageQueue messageQueue;
    private DTUPayService dtuPayService;
    private EventId eventId;
    private boolean success;
    private Exception exception;
    private TokenGenerationRequested tokenGenerationRequested;
    private TokenId[] tokenIds;
    private Collection<TokenId> returnedTokenIds;

    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        dtuPayService = new DTUPayService(messageQueue);
    }

    @Given("a token request with a customerId {string} and an amount of {int}")
    public void a_token_request_with_a_customer_id_and_an_amount_of(String id, Integer amount) {
        var customerId = new CustomerId(UUID.fromString(id));
        tokenGenerationRequested = new TokenGenerationRequested(customerId, amount);
    }

    @When("requesting new tokens")
    public void requesting_new_tokens() throws TokenGenerationException{
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            tokenIds = new TokenId[]{new TokenId(UUID.randomUUID())};
            TokenGenerated tokenGenerated = new TokenGenerated(tokenIds);
            Event newEvent = new Event(TOKEN_GENERATED, new Object[]{tokenGenerated, eventId});
            dtuPayService.handleTokenGenerated(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        returnedTokenIds = dtuPayService.generateTokens(tokenGenerationRequested);
    }

    @Then("a TokensGenerationRequested event is sent")
    public void a_tokens_generation_requested_event_is_sent() {
        Event event = new Event(TOKEN_GENERATION_REQUESTED, new Object[]{tokenGenerationRequested, eventId});
        verify(messageQueue).publish(event);
    }

    @Then("tokens are returned")
    public void tokens_are_returned() {
        Assert.assertArrayEquals(tokenIds, returnedTokenIds.toArray(TokenId[]::new));
    }

    @When("requesting too many tokens")
    public void requesting_too_many_tokens() {
        doAnswer(invocationOnMock -> {
            Event event = invocationOnMock.getArgument(0);
            eventId = event.getArgument(1, EventId.class);
            TokenGenerationFailed tokenGenerationFailed = new TokenGenerationFailed("noOfTokens must be between 1 and 6");
            Event newEvent = new Event(TOKEN_GENERATION_FAILED, new Object[]{tokenGenerationFailed, eventId});
            dtuPayService.handleTokenGenerationFailed(newEvent);
            return null;
        }).when(messageQueue).publish(any(Event.class));

        try {
            returnedTokenIds = dtuPayService.generateTokens(tokenGenerationRequested);
            success = true;
        } catch (TokenGenerationException e) {
            success = false;
            exception = e;
        }
    }

    @Then("an error is returned")
    public void an_error_is_returned() {
        Assert.assertFalse(success);
        Assert.assertNotNull(exception);
    }
}
