package dtu.pay.events;

import dtu.pay.valueObjects.BankAccountId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantAccountRequested {
    BankAccountId bankAccountId;
}
