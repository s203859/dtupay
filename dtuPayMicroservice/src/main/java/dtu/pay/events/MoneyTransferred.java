package dtu.pay.events;

import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;
import dtu.pay.valueObjects.TokenId;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class MoneyTransferred {
    PaymentId paymentId;
}

