package dtu.pay.events;

import lombok.Value;

@Value
public class CustomerAccountDeleteFailed {
    String errorMessage;
}
