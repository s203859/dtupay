package dtu.pay.events;

import dtu.pay.valueObjects.CustomerId;
import lombok.Value;

@Value
public class CustomerReportRequested {
    CustomerId customerId;
}
