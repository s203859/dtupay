package dtu.pay.events;

import lombok.Value;

@Value
public class MerchantAccountDeleteFailed {
    String errorMessage;
}
