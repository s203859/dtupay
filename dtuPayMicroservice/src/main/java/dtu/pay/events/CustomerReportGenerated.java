package dtu.pay.events;

import dtu.pay.entities.Payment;
import lombok.Value;

import java.util.Collection;

@Value
public class CustomerReportGenerated {
    Collection<Payment> payments;
}
