package dtu.pay.events;

import dtu.pay.valueObjects.MerchantId;
import lombok.Value;

@Value
public class MerchantReportRequested {
    MerchantId merchantId;
}
