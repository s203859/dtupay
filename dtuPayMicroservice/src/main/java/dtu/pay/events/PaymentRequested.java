package dtu.pay.events;

import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;
import dtu.pay.valueObjects.TokenId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequested {
    TokenId tokenId;
    MerchantId merchantId;
    BigDecimal amount;
}
