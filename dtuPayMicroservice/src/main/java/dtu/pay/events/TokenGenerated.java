package dtu.pay.events;

import dtu.pay.valueObjects.TokenId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenGenerated {
    TokenId[] tokenIds;
}
