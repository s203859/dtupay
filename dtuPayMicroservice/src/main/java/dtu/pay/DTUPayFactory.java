package dtu.pay;

import messaging.implementations.RabbitMqQueue;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class DTUPayFactory {
    static DTUPayService service;

    public synchronized DTUPayService getService() {
        if (service == null) {
            service = new DTUPayService(new RabbitMqQueue("message_queue"));
        }
        return service;
    }

}
