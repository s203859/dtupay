package dtu.pay.valueObjects;

import lombok.Value;

import java.util.UUID;

@Value
public class EventId {
    UUID id;

    public static EventId randomId() { return new EventId(UUID.randomUUID()); }
}
