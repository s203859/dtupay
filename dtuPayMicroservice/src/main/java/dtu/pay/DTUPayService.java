package dtu.pay;

import dtu.pay.entities.Payment;
import dtu.pay.events.*;
import dtu.pay.exceptions.CustomerReportException;
import dtu.pay.exceptions.DeleteCustomerException;
import dtu.pay.exceptions.DeleteMerchantException;
import dtu.pay.exceptions.PaymentException;
import dtu.pay.exceptions.TokenGenerationException;
import dtu.pay.valueObjects.*;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.EventId;
import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Jonas Attrup Rasmussen (s203859)
 */
public class DTUPayService {
    //region Payment
    ConcurrentMap<EventId, CompletableFuture<Object>> paymentEvents;
    public static final String PAYMENT_REQUESTED = "PaymentRequested";
    public static final String MONEY_TRANSFERRED = "MoneyTransferred";
    public static final String MONEY_TRANSFER_FAILED = "MoneyTransferFailed";
    //endregion
    //region Customer
    ConcurrentMap<EventId, CompletableFuture<Object>> customerEvents;
    public static final String CUSTOMER_ACCOUNT_REQUESTED = "CustomerAccountRequested";
    public static final String CUSTOMER_ACCOUNT_CREATED = "CustomerAccountCreated";
    public static final String CUSTOMER_ACCOUNT_DELETE_REQUESTED = "CustomerAccountDeleteRequested";
    public static final String CUSTOMER_ACCOUNT_DELETED = "CustomerAccountDeleted";
    public static final String CUSTOMER_ACCOUNT_DELETE_FAILED = "CustomerAccountDeleteFailed";
    //endregion
    //region Merchant
    ConcurrentMap<EventId, CompletableFuture<Object>> merchantEvents;
    public static final String MERCHANT_ACCOUNT_REQUESTED = "MerchantAccountRequested";
    public static final String MERCHANT_ACCOUNT_CREATED = "MerchantAccountCreated";
    public static final String MERCHANT_ACCOUNT_DELETE_REQUESTED = "MerchantAccountDeleteRequested";
    public static final String MERCHANT_ACCOUNT_DELETED = "MerchantAccountDeleted";
    public static final String MERCHANT_ACCOUNT_DELETE_FAILED = "MerchantAccountDeleteFailed";
    //endregion
    //region Reports
    public static final String MANAGER_REPORT_REQUESTED = "ManagerReportRequested";
    public static final String MANAGER_REPORT_GENERATED = "ManagerReportGenerated";
    public static final String MERCHANT_REPORT_REQUESTED = "MerchantReportRequested";
    public static final String MERCHANT_REPORT_GENERATED = "MerchantReportGenerated";
    public static final String CUSTOMER_USED_TOKENS_REQUESTED = "CustomerUsedTokensRequested";
    public static final String CUSTOMER_USED_TOKENS_FOUND_FAILED = "CustomerUsedTokensFoundFailed";
    public static final String CUSTOMER_REPORT_GENERATED = "CustomerReportGenerated";
    ConcurrentMap<EventId, CompletableFuture<Object>> reportEvents;
    //endregion

    //region Tokens
    ConcurrentMap<EventId, CompletableFuture<Object>> tokensEvents;
    public static final String TOKEN_GENERATION_REQUESTED = "TokenGenerationRequested";
    public static final String TOKEN_GENERATION_FAILED = "TokenGenerationFailed";
    public static final String TOKEN_GENERATED = "TokenGenerated";
    //endregion
    MessageQueue messageQueue;


    public DTUPayService(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
        this.paymentEvents = new ConcurrentHashMap<>();
        this.customerEvents = new ConcurrentHashMap<>();
        this.merchantEvents = new ConcurrentHashMap<>();
        this.reportEvents = new ConcurrentHashMap<>();
        this.tokensEvents = new ConcurrentHashMap<>();

        messageQueue.addHandler(MONEY_TRANSFERRED, this::handleMoneyTransferred);
        messageQueue.addHandler(MONEY_TRANSFER_FAILED, this::handleMoneyTransferFailed);

        messageQueue.addHandler(CUSTOMER_ACCOUNT_CREATED, this::handleCustomerAccountCreated);
        messageQueue.addHandler(CUSTOMER_ACCOUNT_DELETED, this::handleCustomerAccountDeleted);
        messageQueue.addHandler(CUSTOMER_ACCOUNT_DELETE_FAILED, this::handleCustomerAccountDeleteFailed);

        messageQueue.addHandler(MERCHANT_ACCOUNT_CREATED, this::handleMerchantAccountCreated);
        messageQueue.addHandler(MERCHANT_ACCOUNT_DELETED, this::handleMerchantAccountDeleted);
        messageQueue.addHandler(MERCHANT_ACCOUNT_DELETE_FAILED, this::handleMerchantAccountDeleteFailed);

        messageQueue.addHandler(MANAGER_REPORT_GENERATED, this::handleManagerReportGenerated);
        messageQueue.addHandler(MERCHANT_REPORT_GENERATED, this::handleMerchantReportGenerated);
        messageQueue.addHandler(CUSTOMER_REPORT_GENERATED, this::handleCustomerReportGenerated);
        messageQueue.addHandler(CUSTOMER_USED_TOKENS_FOUND_FAILED, this::handleCustomerUsedTokensFoundFailed);

        messageQueue.addHandler(TOKEN_GENERATED, this::handleTokenGenerated);
        messageQueue.addHandler(TOKEN_GENERATION_FAILED, this::handleTokenGenerationFailed);
    }


    //region Payment
    public PaymentId requestPayment(PaymentRequested paymentRequested) throws PaymentException {
        var eventId = EventId.randomId();
        System.out.println("requestPayment" + ", eventId: " + eventId.getId().toString());
        Event event = new Event(PAYMENT_REQUESTED, new Object[]{paymentRequested, eventId});
        paymentEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        var future = paymentEvents.get(eventId).join();
        if (future instanceof String) {
            throw new PaymentException((String) future);
        }
        return (PaymentId) paymentEvents.get(eventId).join();
    }

    public void handleMoneyTransferred(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleMoneyTransferred" + ", eventId: " + eventId.getId().toString());
        MoneyTransferred moneyTransferred = event.getArgument(0, MoneyTransferred.class);
        paymentEvents.get(eventId).complete(moneyTransferred.getPaymentId());
    }

    public void handleMoneyTransferFailed(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleMoneyTransferFailed" + ", eventId: " + eventId.getId().toString());
        MoneyTransferFailed moneyTransferFailed = event.getArgument(0, MoneyTransferFailed.class);
        paymentEvents.get(eventId).complete(moneyTransferFailed.getErrorMessage());
    }
    //endregion

    //region Customer
    public CustomerId createCustomer(CustomerAccountRequested customerAccountRequested) {
        var eventId = EventId.randomId();
        System.out.println("createCustomer" + ", eventId: " + eventId.getId().toString());
        Event event = new Event(CUSTOMER_ACCOUNT_REQUESTED, new Object[]{customerAccountRequested, eventId});
        customerEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        return (CustomerId) customerEvents.get(eventId).join();
    }

    public void handleCustomerAccountCreated(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleCustomerAccountCreated" + ", eventId: " + eventId.getId().toString());
        CustomerAccountCreated customerAccountCreated = event.getArgument(0, CustomerAccountCreated.class);
        customerEvents.get(eventId).complete(customerAccountCreated.getCustomerId());
    }

    public void deleteCustomer(CustomerAccountDeleteRequested customerAccountDeleteRequested) throws DeleteCustomerException {
        var eventId = EventId.randomId();
        System.out.println("deleteCustomer" + ", eventId: " + eventId.getId().toString());
        Event event = new Event(CUSTOMER_ACCOUNT_DELETE_REQUESTED, new Object[]{customerAccountDeleteRequested, eventId});
        customerEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        Object completion = customerEvents.get(eventId).join();
        if (completion != null) {
            throw new DeleteCustomerException((String) completion);
        }
    }

    public void handleCustomerAccountDeleted(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleCustomerAccountDeleted" + ", eventId: " + eventId.getId().toString());
        customerEvents.get(eventId).complete(null);
    }

    public void handleCustomerAccountDeleteFailed(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleCustomerAccountDeleteFailed" + ", eventId: " + eventId.getId().toString());
        CustomerAccountDeleteFailed customerAccountDeleteFailed = event.getArgument(0, CustomerAccountDeleteFailed.class);
        customerEvents.get(eventId).complete(customerAccountDeleteFailed.getErrorMessage());
    }
    //endregion

    //region Merchant
    public MerchantId createMerchant(MerchantAccountRequested merchantAccountRequested) {
        var eventId = EventId.randomId();
        System.out.println("createMerchant" + ", eventId: " + eventId.getId().toString());
        Event event = new Event(MERCHANT_ACCOUNT_REQUESTED, new Object[]{merchantAccountRequested, eventId});
        merchantEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        return (MerchantId) merchantEvents.get(eventId).join();
    }

    public void handleMerchantAccountCreated(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleMerchantAccountCreated" + ", eventId: " + eventId.getId().toString());
        MerchantAccountCreated merchantAccountCreated = event.getArgument(0, MerchantAccountCreated.class);
        merchantEvents.get(eventId).complete(merchantAccountCreated.getMerchantId());
    }

    public void deleteMerchant(MerchantAccountDeleteRequested merchantAccountDeleteRequested) throws DeleteMerchantException {
        var eventId = EventId.randomId();
        System.out.println("deleteMerchant" + ", eventId: " + eventId.getId().toString());
        Event event = new Event(MERCHANT_ACCOUNT_DELETE_REQUESTED, new Object[]{merchantAccountDeleteRequested, eventId});
        merchantEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        Object completion = merchantEvents.get(eventId).join();
        if (completion != null) {
            throw new DeleteMerchantException((String) completion);
        }
    }

    public void handleMerchantAccountDeleted(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleMerchantAccountDeleted" + ", eventId: " + eventId.getId().toString());
        merchantEvents.get(eventId).complete(null);
    }

    public void handleMerchantAccountDeleteFailed(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleMerchantAccountDeleteFailed" + ", eventId: " + eventId.getId().toString());
        MerchantAccountDeleteFailed merchantAccountDeleteFailed = event.getArgument(0, MerchantAccountDeleteFailed.class);
        merchantEvents.get(eventId).complete(merchantAccountDeleteFailed.getErrorMessage());
    }
    //endregion

    //region Reports
    public Collection<Payment> generateManagerReport() {
        var eventId = EventId.randomId();
        System.out.println("generateManagerReport" + ", eventId: " + eventId.getId().toString());
        var event = new Event(MANAGER_REPORT_REQUESTED, new Object[]{new ManagerReportRequested(), eventId});
        reportEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        return (Collection<Payment>) reportEvents.get(eventId).join();
    }

    public void handleManagerReportGenerated(Event event) {
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("handleManagerReportGenerated" + ", eventId: " + eventId.getId().toString());
        var payload = event.getArgument(0, ManagerReportGenerated.class);
        reportEvents.get(eventId).complete(payload.getPayments());
    }

    public Collection<Payment> generateMerchantReport(MerchantReportRequested merchantReportRequested) {
        var eventId = EventId.randomId();
        System.out.println("generateMerchantReport" + ", eventId: " + eventId.getId().toString());
        var event = new Event(MERCHANT_REPORT_REQUESTED, new Object[]{merchantReportRequested, eventId});
        reportEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        return (Collection<Payment>) reportEvents.get(eventId).join();
    }

    public void handleMerchantReportGenerated(Event event) {
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("handleMerchantReportGenerated" + ", eventId: " + eventId.getId().toString());
        var payload = event.getArgument(0, MerchantReportGenerated.class);
        reportEvents.get(eventId).complete(payload.getPayments());
    }

    public Collection<Payment> generateCustomerReport(CustomerReportRequested customerReportRequested) throws CustomerReportException {
        var eventId = EventId.randomId();
        System.out.println("generateCustomerReport" + ", eventId: " + eventId.getId().toString());
        var request = new CustomerUsedTokensRequested(customerReportRequested.getCustomerId());
        var event = new Event(CUSTOMER_USED_TOKENS_REQUESTED, new Object[]{request, eventId});
        reportEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        var future = reportEvents.get(eventId).join();
        if (future instanceof String) {
            throw new CustomerReportException((String) future);
        }
        return (Collection<Payment>) reportEvents.get(eventId).join();
    }

    public void handleCustomerReportGenerated(Event event) {
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("handleCustomerReportGenerated" + ", eventId: " + eventId.getId().toString());
        var payload = event.getArgument(0, CustomerReportGenerated.class);
        reportEvents.get(eventId).complete(payload.getPayments());
    }

    public void handleCustomerUsedTokensFoundFailed(Event event) {
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("handleCustomerUsedTokensFoundFailed" + ", eventId: " + eventId.getId().toString());
        var payload = event.getArgument(0, CustomerUsedTokensFoundFailed.class);
        reportEvents.get(eventId).complete(payload.getErrorMessage());
    }
    //endregion

    //region Tokens
    public Collection<TokenId> generateTokens(TokenGenerationRequested tokenGenerationRequested) throws TokenGenerationException {
        var eventId = EventId.randomId();
        System.out.println("generateTokens" + ", eventId: " + eventId.getId().toString());
        Event event = new Event(TOKEN_GENERATION_REQUESTED, new Object[]{tokenGenerationRequested, eventId});
        tokensEvents.put(eventId, new CompletableFuture<>());
        messageQueue.publish(event);
        var result = tokensEvents.get(eventId).join();
        if (result instanceof String) {
            throw new TokenGenerationException((String) result);
        }
        return (Collection<TokenId>) result;
    }

    public void handleTokenGenerated(Event event) {
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("handleTokenGenerated" + ", eventId: " + eventId.getId().toString());
        var payload = event.getArgument(0, TokenGenerated.class);
        tokensEvents.get(eventId).complete(Arrays.asList(payload.getTokenIds()));
    }

    public void handleTokenGenerationFailed(Event event) {
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("handleTokenGenerationFailed" + ", eventId: " + eventId.getId().toString());
        var payload = event.getArgument(0, TokenGenerationFailed.class);
        tokensEvents.get(eventId).complete(payload.getErrorMessage());
    }

    //endregion
}
