package dtu.pay.entities;

import dtu.pay.valueObjects.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Payment {
    PaymentId paymentId;
    TokenId tokenId;
    CustomerId customerId;
    MerchantId merchantId;
    BankAccountId customerBankAccountId;
    BankAccountId merchantBankAccountId;
    BigDecimal amount;
}
