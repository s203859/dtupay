package dtu.pay;

import dtu.pay.events.*;
import dtu.pay.exceptions.*;
import dtu.pay.valueObjects.*;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.jboss.resteasy.spi.HttpResponseCodes;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
@Path("/")
public class DTUPayResource {
    private final DTUPayService dtuPayService = new DTUPayFactory().getService();

    //region Customer
    @Path("customers")
    @POST
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Customer created",
                    content = @Content(schema = @Schema(implementation = CustomerId.class)))
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomerJson(String bankAccountId) {
        CustomerAccountRequested customerAccountRequested = new CustomerAccountRequested(new BankAccountId(bankAccountId));
        CustomerId customerId = dtuPayService.createCustomer(customerAccountRequested);
        return Response.status(HttpResponseCodes.SC_CREATED)
                .entity(customerId)
                .build();
    }

    @Path("customers/{id}")
    @DELETE
    @APIResponses({
            @APIResponse(responseCode = "204", description = "Customer deleted"),
            @APIResponse(responseCode = "404", description = "Customer not found",
                    content = @Content(schema = @Schema(implementation = String.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCustomerJson(@PathParam("id") String id) {
        try {
            CustomerId customerId = new CustomerId(UUID.fromString(id));
            CustomerAccountDeleteRequested customerAccountDeleteRequested = new CustomerAccountDeleteRequested(customerId);
            dtuPayService.deleteCustomer(customerAccountDeleteRequested);
            return Response.status(HttpResponseCodes.SC_NO_CONTENT).build();
        } catch (DeleteCustomerException exception) {
            return Response.status(HttpResponseCodes.SC_NOT_FOUND)
                    .entity(exception.getMessage())
                    .build();
        }
    }

    @Path("customers/payments/{id}")
    @GET
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Customer report generated",
                    content = @Content(schema = @Schema(implementation = Collection.class))),
            @APIResponse(responseCode = "400", description = "Customer report generation failed",
                    content = @Content(schema = @Schema(implementation = String.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerPaymentsJson(@PathParam("id") String idParam) {
        var id = new CustomerId(UUID.fromString(idParam));
        var request = new CustomerReportRequested(id);
        try {
            var payments = dtuPayService.generateCustomerReport(request);
            return Response.status(HttpResponseCodes.SC_OK)
                    .entity(payments)
                    .build();
        } catch (CustomerReportException exception) {
            return Response.status(HttpResponseCodes.SC_BAD_REQUEST)
                    .entity(exception.getMessage())
                    .build();
        }
    }

    @Path("customers/tokens")
    @POST
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Tokens generated",
                    content = @Content(schema = @Schema(implementation = Collection.class))),
            @APIResponse(responseCode = "400", description = "Tokens generation failed",
                    content = @Content(schema = @Schema(implementation = String.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateTokensJson(TokenGenerationRequested tokenGenerationRequested) {
        try {
            var tokens = dtuPayService.generateTokens(tokenGenerationRequested);
            return Response.status(HttpResponseCodes.SC_CREATED)
                    .entity(tokens)
                    .build();
        } catch (TokenGenerationException e) {
            return Response.status(HttpResponseCodes.SC_BAD_REQUEST)
                    .entity(e.getMessage())
                    .build();
        }
    }

    //endregion

    //region Merchant
    @Path("merchants/payments")
    @POST
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Payment created",
                    content = @Content(schema = @Schema(implementation = PaymentId.class))),
            @APIResponse(responseCode = "400", description = "Payment failed",
                    content = @Content(schema = @Schema(implementation = String.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPaymentJson(PaymentRequested paymentRequested) {
        try {
            PaymentId paymentId = dtuPayService.requestPayment(paymentRequested);
            return Response.status(HttpResponseCodes.SC_CREATED)
                    .entity(paymentId)
                    .build();
        } catch (PaymentException exception) {
            return Response.status(HttpResponseCodes.SC_BAD_REQUEST)
                    .entity(exception.getMessage())
                    .build();
        }
    }

    @Path("merchants")
    @POST
    @APIResponses({
            @APIResponse(responseCode = "201", description = "Merchant created",
                    content = @Content(schema = @Schema(implementation = MerchantId.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMerchantJson(String bankAccountId) {
        if (bankAccountId == null || bankAccountId.isEmpty())
            return Response.status(HttpResponseCodes.SC_BAD_REQUEST).build();

        MerchantAccountRequested merchantAccountRequested = new MerchantAccountRequested(new BankAccountId(bankAccountId));
        MerchantId merchantId = dtuPayService.createMerchant(merchantAccountRequested);
        return Response.status(HttpResponseCodes.SC_CREATED)
                .entity(merchantId)
                .build();
    }

    @Path("merchants/{id}")
    @DELETE
    @APIResponses({
            @APIResponse(responseCode = "204", description = "Merchant deleted"),
            @APIResponse(responseCode = "404", description = "Merchant not found",
                    content = @Content(schema = @Schema(implementation = String.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMerchantJson(@PathParam("id") String id) {
        try {
            MerchantId merchantId = new MerchantId(UUID.fromString(id));
            MerchantAccountDeleteRequested merchantAccountDeleteRequested = new MerchantAccountDeleteRequested(merchantId);
            dtuPayService.deleteMerchant(merchantAccountDeleteRequested);
            return Response.status(HttpResponseCodes.SC_NO_CONTENT).build();
        } catch (DeleteMerchantException exception) {
            return Response.status(HttpResponseCodes.SC_NOT_FOUND)
                    .entity(exception.getMessage())
                    .build();
        }
    }

    @Path("merchants/payments/{id}")
    @GET
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Merchant report generated",
                    content = @Content(schema = @Schema(implementation = Collection.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMerchantPaymentsJson(@PathParam("id") String idParam) {
        var id = new MerchantId(UUID.fromString(idParam));
        var request = new MerchantReportRequested(id);
        var payments = dtuPayService.generateMerchantReport(request);
        return Response.status(HttpResponseCodes.SC_OK)
                .entity(payments)
                .build();
    }
    //endregion

    //region Manager
    @Path("managers/payments")
    @GET
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Manager report generated",
                    content = @Content(schema = @Schema(implementation = Collection.class))),
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getManagerPaymentsJson() {
        var payments = dtuPayService.generateManagerReport();
        return Response.status(HttpResponseCodes.SC_OK)
                .entity(payments)
                .build();
    }
    //endregion
}
