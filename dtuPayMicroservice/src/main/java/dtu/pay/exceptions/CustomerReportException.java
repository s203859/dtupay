package dtu.pay.exceptions;

public class CustomerReportException extends Exception{
    public CustomerReportException(String message) { super(message); }
}
