package dtu.pay.exceptions;

public class DeleteMerchantException extends Exception{
    public DeleteMerchantException(String message) { super(message); }
}
