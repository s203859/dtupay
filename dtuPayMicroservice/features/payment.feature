# @author Emil Arnika Skydsgaard (s193579)

Feature: Payment

  Scenario: PaymentRequested event is sent
    Given a payment request for amount 42
    And tokenId "f01c269d-109b-40e5-b380-4d92991e5910"
    And merchantId "114e20d4-9a6a-4db1-afc6-711abf838fdb"
    When a payment is requested
    Then a PaymentRequested event is sent

  Scenario: MoneyTransferred event results in successful payment
    Given a payment request for amount 42
    And tokenId "f01c269d-109b-40e5-b380-4d92991e5910"
    And merchantId "114e20d4-9a6a-4db1-afc6-711abf838fdb"
    And the payment has been requested
    When a MoneyTransferred event is received
    Then the paymentId of the payment is returned to the caller

  Scenario: A payment is requested returns new payment id
    Given PaymentRequested
    When creating the payment
    Then the payment id is returned

  Scenario: A payment is requested sends MoneyTransferred event
    Given PaymentRequested
    When creating the payment
    Then a MoneyTransferred event is sent

  Scenario: A payment is requested returns error message
    Given PaymentRequested
    When creating the invalid payment
    Then the money transfer is not successful
    And it has an error message
