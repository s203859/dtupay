# @author Jonas Løvenhardt Henriksen (s195457)

Feature: Token

  Scenario: A user token request results in a TokensGenerationRequested event
  Given a token request with a customerId "566acc4e-80eb-4d1f-96b1-4d954e6021d6" and an amount of 4
  When requesting new tokens
  Then a TokensGenerationRequested event is sent

  Scenario: A user token request returns new tokens
    Given a token request with a customerId "566acc4e-80eb-4d1f-96b1-4d954e6021d6" and an amount of 4
    When requesting new tokens
    Then tokens are returned

  Scenario: An invalid user token request returns an error
    Given a token request with a customerId "566acc4e-80eb-4d1f-96b1-4d954e6021d6" and an amount of 8
    When requesting too many tokens
    Then an error is returned
