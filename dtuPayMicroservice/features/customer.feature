# @author Martin Posselt Munck (s213791)

Feature: Customer
  Scenario: A new customer is requested returns new customer id
    Given a CustomerAccountRequested with bank id "f01c269d-109b-40e5-b380-4d92991e5910"
    When creating a new customer
    Then a new customer id is returned

  Scenario: Creating a new customer sends CustomerAccountRequested event
    Given a CustomerAccountRequested with bank id "f01c269d-109b-40e5-b380-4d92991e5910"
    When creating a new customer
    Then a CustomerAccountRequested is sent

  Scenario: Delete existing customer requested succeeds
    Given a CustomerAccountDeleteRequested with customer id "f01c269d-109b-40e5-b380-4d92991e5910"
    When deleting an existing customer
    Then the customer is deleted

  Scenario: Delete existing customer requested fails
    Given a CustomerAccountDeleteRequested with customer id "f01c269d-109b-40e5-b380-4d92991e5910"
    When deleting a non existing customer
    Then the customer deletion failed