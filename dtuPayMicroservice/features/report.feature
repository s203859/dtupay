# @author Jonas Attrup Rasmussen (s203859)

Feature: report
  Scenario: A manager report is requested returns list of all payments
    Given ManagerReportRequested
    When creating the manager report
    Then all payments are returned

  Scenario: A manager report is requested sends ManagerReportRequested
    Given ManagerReportRequested
    When creating the manager report
    Then a ManagerReportRequested event is sent

  Scenario: A merchant report is requested returns list of all payments for the merchant
    Given a merchant with merchantId "f01c269d-109b-40e5-b380-4d92991e5910"
    And MerchantReportRequested
    When creating the merchant report
    Then all payments with the merchantId are returned

  Scenario: A merchant report is requested sends MerchantReportRequested
    Given MerchantReportRequested
    When creating the merchant report
    Then a MerchantReportRequested event is sent

  Scenario: A customer report is requested returns list of all payments for the customer
    Given a customer with customerId "f01c269d-109b-40e5-b380-4d92991e5910"
    And CustomerReportRequested
    When creating the customer report
    Then all payments with the tokens from the customer Id are returned

  Scenario: A customer report is requested sends CustomerReportRequested
    Given CustomerReportRequested
    When creating the customer report
    Then a CustomerUsedTokensRequested event is sent

  Scenario: A customer report is requested throws exception
    Given a customer with customerId "f01c269d-109b-40e5-b380-4d92991e5910"
    And CustomerReportRequested
    When creating the customer report for customer without used tokens
    Then it is not successful
    And an exception with error message "Customer with id f01c269d-109b-40e5-b380-4d92991e5910 does not have any used tokens""