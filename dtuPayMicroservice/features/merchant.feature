# @author Tomas Hagenau Andersen (s233489)

Feature: Merchant
  Scenario: A new merchant is requested returns new merchant id
    Given a MerchantAccountRequested with bank id "f01c269d-109b-40e5-b380-4d92991e5910"
    When creating a new merchant
    Then a new merchant id is returned

  Scenario: Creating a new merchant sends MerchantAccountRequested even
    Given a MerchantAccountRequested with bank id "f01c269d-109b-40e5-b380-4d92991e5910"
    When creating a new merchant
    Then a MerchantAccountRequested is sent

  Scenario: Delete existing merchant requested succeeds
    Given a MerchantAccountDeleteRequested with merchant id "f01c269d-109b-40e5-b380-4d92991e5910"
    When deleting an existing merchant
    Then the merchant is deleted

  Scenario: Delete existing merchant requested fails
    Given a MerchantAccountDeleteRequested with merchant id "f01c269d-109b-40e5-b380-4d92991e5910"
    When deleting a non existing merchant
    Then the merchant deletion failed