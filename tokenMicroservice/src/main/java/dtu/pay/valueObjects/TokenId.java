package dtu.pay.valueObjects;

import lombok.Value;

import java.util.UUID;

@Value
public class TokenId implements Comparable<TokenId> {
    public static TokenId randomId() { return new TokenId(UUID.randomUUID()); }

    UUID id;

    @Override
    public int compareTo(TokenId o) {
        return this.getId().compareTo(o.getId());
    }
}
