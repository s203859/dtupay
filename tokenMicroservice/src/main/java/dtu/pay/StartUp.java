package dtu.pay;

import messaging.implementations.RabbitMqQueue;

/**
 * @author Jonas Løvenhardt Henriksen (s195457)
 */
public class StartUp {
    public static void main(String[] args) {
        new StartUp().startUp();
    }

    private void startUp() {
        var mq = new RabbitMqQueue("message_queue");
        var repo = new InMemoryTokenRepository();
        new TokenService(mq, repo);
    }
}
