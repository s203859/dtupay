package dtu.pay.entities;

import dtu.pay.valueObjects.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class Token {
    public Token(CustomerId customerId) {
        this.tokenId = TokenId.randomId();
        this.customerId = customerId;
        this.used = false;
    }

    TokenId tokenId;
    CustomerId customerId;
    boolean used;
}
