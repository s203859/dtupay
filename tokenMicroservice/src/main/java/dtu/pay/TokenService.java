package dtu.pay;

import dtu.pay.entities.Token;
import dtu.pay.events.*;
import dtu.pay.valueObjects.EventId;
import io.cucumber.java.an.E;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author Tomas Hagenau Andersen (s233489)
 */
public class TokenService {
    public static final String PAYMENT_CREATED = "PaymentCreated";
    public static final String CUSTOMER_ID_FOUND = "CustomerIdFound";
    public static final String TOKEN_GENERATION_REQUESTED = "TokenGenerationRequested";
    public static final String TOKEN_GENERATION_FAILED = "TokenGenerationFailed";
    public static final String TOKEN_GENERATED = "TokenGenerated";
    public static final String CUSTOMER_ID_FOUND_FAILED = "CustomerIdFoundFailed";
    public static final String CUSTOMER_USED_TOKENS_REQUESTED = "CustomerUsedTokensRequested";
    public static final String CUSTOMER_USED_TOKENS_FOUND = "CustomerUsedTokensFound";
    public static final String CUSTOMER_USED_TOKENS_FOUND_FAILED = "CustomerUsedTokensFoundFailed";
    MessageQueue messageQueue;
    ITokenRepository repository;

    public TokenService(MessageQueue messageQueue, ITokenRepository repository) {
        this.messageQueue = messageQueue;
        this.repository = repository;

        messageQueue.addHandler(PAYMENT_CREATED, this::handlePaymentCreated);
        messageQueue.addHandler(TOKEN_GENERATION_REQUESTED, this::handleGenerateTokenRequested);
        messageQueue.addHandler(CUSTOMER_USED_TOKENS_REQUESTED, this::handleCustomerUsedTokensRequested);
    }

    private void publishFailure(EventId eventId, String errorMessage) {
        TokenGenerationFailed tokenGenerationFailed = new TokenGenerationFailed(errorMessage);
        Event newEvent = new Event(TOKEN_GENERATION_FAILED, new Object[] { tokenGenerationFailed, eventId });
        messageQueue.publish(newEvent);
    }
    public void handleGenerateTokenRequested(Event event) {
        TokenGenerationRequested generateTokenRequested = event.getArgument(0, TokenGenerationRequested.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handleGenerateTokenRequested" + ", eventId: " + eventId.getId().toString());

        // Fail on invalid number of requested tokens
        if (generateTokenRequested.getNoOfTokens() < 1 || generateTokenRequested.getNoOfTokens() > 5) {
            publishFailure(eventId, "noOfTokens must be between 1 and 5");
            return;
        }

        // Fail on requesting too many tokens
        if (repository.noOfActiveTokens(generateTokenRequested.getCustomerId()) > 1) {
            publishFailure(eventId, "Token limit exceeded");
            return;
        }

        // Everything is good. Generate the new tokens and publish an event.
        var tokenIds = repository.createTokens(generateTokenRequested.getCustomerId(), generateTokenRequested.getNoOfTokens());
        System.out.println("Generated tokens ");
        System.out.println(Arrays.stream(tokenIds)
                .map(x -> x.getId().toString())
                .collect(Collectors.joining(", ")));
        TokenGenerated tokenGenerated = new TokenGenerated(tokenIds);
        Event newEvent = new Event(TOKEN_GENERATED, new Object[] { tokenGenerated, eventId });
        messageQueue.publish(newEvent);
    }

    public void handlePaymentCreated(Event event) {
        PaymentCreated paymentCreated = event.getArgument(0, PaymentCreated.class);
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("handlePaymentCreated" + ", eventId: " + eventId.getId().toString());
        System.out.println("With token id: " + paymentCreated.getTokenId().toString());
        Token token = repository.getToken(paymentCreated.getTokenId());

        if (token == null || token.isUsed()) {
            CustomerIdFoundFailed customerIdFoundFailed =
                    new CustomerIdFoundFailed("Customer token does not exist or have been used");
            Event newEvent = new Event(CUSTOMER_ID_FOUND_FAILED, new Object[]{ customerIdFoundFailed, eventId });
            messageQueue.publish(newEvent);
            System.out.println("handlePaymentCreated sent CUSTOMER_ID_FOUND_FAILED");
            return;
        }

        repository.setUsed(token.getTokenId());

        System.out.println("Event Contents:");
        System.out.println("-" + paymentCreated.getTokenId() + " " + paymentCreated.getMerchantId());

        // Send new event
        CustomerIdFound customerIdFound = new CustomerIdFound(paymentCreated.getPaymentId(), token.getCustomerId());
        Event newEvent = new Event(CUSTOMER_ID_FOUND, new Object[]{ customerIdFound, eventId });
        messageQueue.publish(newEvent);
    }

    public void handleCustomerUsedTokensRequested(Event event){
        var customerUsedTokensRequested = event.getArgument(0, CustomerUsedTokensRequested.class);
        var eventId = event.getArgument(1, EventId.class);
        System.out.println("CustomerUsedTokensRequested received"  + ", eventId: " + eventId.getId().toString());
        var tokenIds = repository.getUsedTokenIdsForCustomer(customerUsedTokensRequested.getCustomerId());

        if (tokenIds.length > 0){
            var customerUsedTokensFound = new CustomerUsedTokensFound(tokenIds);
            var newEvent = new Event(CUSTOMER_USED_TOKENS_FOUND, new Object[] { customerUsedTokensFound, eventId });
            messageQueue.publish(newEvent);
        }
        else{
            var customerId = customerUsedTokensRequested.getCustomerId().getId().toString();
            var customerUsedTokensFailed =
                    new CustomerUsedTokensFoundFailed(
                            String.format("Customer with id %s does not have any used tokens", customerId));
            var newEvent = new Event(CUSTOMER_USED_TOKENS_FOUND_FAILED,
                    new Object[] { customerUsedTokensFailed, eventId });
            messageQueue.publish(newEvent);
        }
    }
}
