package dtu.pay;

import dtu.pay.entities.Token;
import dtu.pay.valueObjects.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Jonas Attrup Rasmussen (s203859)
 */
public class InMemoryTokenRepository implements ITokenRepository {
    ConcurrentMap<TokenId, Token> tokens;

    public InMemoryTokenRepository() {
        this.tokens = new ConcurrentHashMap<>();
    }

    @Override
    public TokenId[] createTokens(CustomerId customerId, int numberOfTokens) {
        var tokenIds = new TokenId[numberOfTokens];

        for (int i = 0; i < numberOfTokens; i++) {
            var token = new Token(customerId);
            tokens.put(token.getTokenId(), token);
            tokenIds[i] = token.getTokenId();
        }

        return Arrays
                .stream(tokenIds)
                .sorted().
                toArray(TokenId[]::new);
    }

    @Override
    public long noOfActiveTokens(CustomerId customerId) {
        return tokens
                .values()
                .stream()
                .filter(t -> t.getCustomerId().equals(customerId) && !t.isUsed())
                .count();
    }

    @Override
    public TokenId[] getUnusedTokenIdsForCustomer(CustomerId customerId) {
        ArrayList<TokenId> ids = new ArrayList<>();
        for (var token : tokens.values()) {
            if (token.getCustomerId().equals(customerId) && !token.isUsed()) {
                ids.add(token.getTokenId());
            }
        }
        return ids
                .stream()
                .sorted()
                .toArray(TokenId[]::new);
    }

    @Override
    public TokenId[] getUsedTokenIdsForCustomer(CustomerId customerId) {
        ArrayList<TokenId> ids = new ArrayList<>();
        for (var token : tokens.values()) {
            if (token.getCustomerId().equals(customerId) && token.isUsed()) {
                ids.add(token.getTokenId());
            }
        }
        return ids
                .stream()
                .sorted()
                .toArray(TokenId[]::new);
    }

    @Override
    public Token getToken(TokenId tokenId) {
        System.out.println("Called get token with tokenId " + tokenId.getId().toString());
        for (var token : tokens.keySet()) {
            System.out.println(token.getId());
        }
        return tokens.get(tokenId);
    }

    @Override
    public void setUsed(TokenId tokenId) {
        tokens.get(tokenId).setUsed(true);
    }
}
