package dtu.pay.events;

import dtu.pay.valueObjects.TokenId;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
public class CustomerUsedTokensFoundFailed {
    String errorMessage;
}
