package dtu.pay.events;

import lombok.Value;

@Value
public class TokenGenerationFailed {
    String errorMessage;
}
