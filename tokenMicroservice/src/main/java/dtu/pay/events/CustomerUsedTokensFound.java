package dtu.pay.events;

import dtu.pay.valueObjects.TokenId;
import lombok.Value;

@Value
public class CustomerUsedTokensFound {
    TokenId[] tokenIds;
}
