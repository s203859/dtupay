package dtu.pay.events;

import dtu.pay.valueObjects.CustomerId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenGenerationRequested {
    CustomerId customerId;
    int noOfTokens;
}
