package dtu.pay;

import dtu.pay.entities.Token;
import dtu.pay.valueObjects.*;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Emil Arnika Skydsgaard (s193579)
 */
public interface ITokenRepository {
    TokenId[] createTokens(CustomerId customerId, int numberOfTokens);
    long noOfActiveTokens(CustomerId customerId);
    TokenId[] getUnusedTokenIdsForCustomer(CustomerId customerId);

    TokenId[] getUsedTokenIdsForCustomer(CustomerId customerId);

    Token getToken(TokenId tokenId);
    void setUsed(TokenId tokenId);
}
