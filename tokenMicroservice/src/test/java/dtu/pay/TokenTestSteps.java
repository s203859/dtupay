package dtu.pay;

import dtu.pay.events.*;
import dtu.pay.valueObjects.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class TokenTestSteps {
    MessageQueue messageQueue;
    ITokenRepository tokenRepository;
    TokenService tokenService;

    CustomerId customerId;
    TokenId[] tokenIds;
    EventId eventId;
    PaymentId paymentId;
    CustomerUsedTokensRequested customerUsedTokensRequested;
    Event event;

    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        tokenRepository = new InMemoryTokenRepository();
        tokenService = new TokenService(messageQueue, tokenRepository);
    }

    @Given("an existing unused token for a Customer with customerId {string}")
    public void an_existing_unused_token_for_a_customer_with_customer_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
        tokenIds = tokenRepository.createTokens(this.customerId, 1);
    }
    @When("a PaymentCreated event containing that token is received")
    public void a_payment_created_event_containing_that_token_is_received() {
        eventId = new EventId(UUID.randomUUID());
        paymentId = new PaymentId(UUID.randomUUID());
        PaymentCreated paymentCreated = new PaymentCreated(paymentId, tokenIds[0], new MerchantId(UUID.randomUUID()));
        Event event = new Event(TokenService.PAYMENT_CREATED, new Object[]{ paymentCreated, eventId });
        tokenService.handlePaymentCreated(event);
    }
    @Then("the token is marked as used")
    public void the_token_is_marked_as_used() {
        Assert.assertTrue(tokenRepository.getToken(tokenIds[0]).isUsed());
    }
    @Then("a CustomerIdFound event is sent with the corresponding customerId")
    public void a_customer_id_found_event_is_sent_with_the_corresponding_customer_id() {
        // Write code here that turns the phrase above into concrete actions
        CustomerIdFound customerIdFound = new CustomerIdFound(paymentId, customerId);
        verify(messageQueue).publish(new Event(TokenService.CUSTOMER_ID_FOUND, new Object[] { customerIdFound, eventId }));
    }

    @Given("a customer with customerId {string}")
    public void a_customer_with_customer_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
    }
    @Given("the customer has {int} unused token")
    public void the_customer_has_unused_token(Integer noOfTokens) {
        tokenIds = tokenRepository.createTokens(this.customerId, noOfTokens);
    }
    @When("a TokensRequested event is received with amount {int}")
    public void a_tokens_requested_event_is_received_with_amount(Integer noOfTokens) {
        eventId = new EventId(UUID.randomUUID());
        TokenGenerationRequested tokenGenerationRequested = new TokenGenerationRequested(this.customerId, noOfTokens);
        Event event = new Event(TokenService.TOKEN_GENERATION_REQUESTED, new Object[] { tokenGenerationRequested, eventId });
        tokenService.handleGenerateTokenRequested(event);
    }
    @Then("a TokensCreated event is sent with the tokenIds")
    public void a_tokens_created_event_is_sent_with_the_token_ids() {
        TokenId[] unusedTokens = tokenRepository.getUnusedTokenIdsForCustomer(this.customerId);
        TokenGenerated tokenGenerated = new TokenGenerated(unusedTokens);
        verify(messageQueue).publish(new Event(TokenService.TOKEN_GENERATED, new Object[]{ tokenGenerated, eventId }));
    }

    @Then("{int} unused tokens exist for the customer")
    public void unused_tokens_exist_for_the_customer(int totalNoOfTokens) {
        Assert.assertEquals(totalNoOfTokens, tokenRepository.noOfActiveTokens(this.customerId));
    }

    @Given("an existing used token for a Customer with customerId {string}")
    public void anExistingUsedTokenForACustomerWithCustomerId(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
        tokenIds = tokenRepository.createTokens(this.customerId, 1);
        tokenRepository.getToken(tokenIds[0]).setUsed(true);
    }

    @Then("a CustomerIdFoundFailed event is sent")
    public void aCustomerIdFoundFailedEventIsSent() {
        CustomerIdFoundFailed customerIdFoundFailed =
                new CustomerIdFoundFailed("Customer token does not exist or have been used");
        verify(messageQueue).publish(new Event(TokenService.CUSTOMER_ID_FOUND_FAILED, new Object[] { customerIdFoundFailed, eventId }));
    }

    @Given("an unknown token for a Customer with customerId {string}")
    public void anUnknownTokenForACustomerWithCustomerId(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
        tokenIds = new TokenId[] { TokenId.randomId() };
    }

    @Then("a TokensGenerationFailed event is sent with message {string}")
    public void aTokensGenerationFailedEventIsSentWithMessage(String errorMessage) {
        TokenGenerationFailed tokenGenerationFailed = new TokenGenerationFailed(errorMessage);
        verify(messageQueue).publish(new Event(TokenService.TOKEN_GENERATION_FAILED, new Object[]{ tokenGenerationFailed, eventId }));
    }

    @Given("a CustomerUsedTokensRequested event with customerId {string}")
    public void a_customer_used_tokens_requested_event_with_customer_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
        this.customerUsedTokensRequested = new CustomerUsedTokensRequested(this.customerId);
    }
    @Given("the customer has two used tokens")
    public void the_customer_has_two_used_tokens() {
        tokenIds = tokenRepository.createTokens(this.customerId, 2);
        tokenRepository.getToken(tokenIds[0]).setUsed(true);
        tokenRepository.getToken(tokenIds[1]).setUsed(true);
    }
    @When("a CustomerUsedTokensRequested event is received")
    public void a_customer_used_tokens_requested_event_is_received() {
        eventId = new EventId(UUID.randomUUID());
        Event event = new Event(TokenService.CUSTOMER_USED_TOKENS_REQUESTED,
                new Object[]{ customerUsedTokensRequested, eventId});
        tokenService.handleCustomerUsedTokensRequested(event);
    }
    @Then("a CustomerUsedTokensFound event is sent")
    public void a_customer_used_tokens_found_event_is_sent() {
        CustomerUsedTokensFound customerUsedTokensFound =
                new CustomerUsedTokensFound(tokenRepository.getUsedTokenIdsForCustomer(this.customerId));
        event = new Event(TokenService.CUSTOMER_USED_TOKENS_FOUND, new Object[]{ customerUsedTokensFound, eventId});
        verify(messageQueue).publish(event);
    }
    @Then("the event contains the two used tokens")
    public void the_event_contains_the_two_used_tokens() {
        TokenId[] customerTokenIds = event.getArgument(0, CustomerUsedTokensFound.class).getTokenIds();
        Assert.assertEquals(tokenIds.length, customerTokenIds.length);
        Assert.assertTrue(Arrays.stream(customerTokenIds).toList().containsAll(List.of(customerTokenIds)));
    }
    @Given("a CustomerUsedTokensRequested event with customerId {string} without any used tokens")
    public void a_customer_used_tokens_requested_event_with_customer_id_without_any_used_tokens(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
        this.customerUsedTokensRequested = new CustomerUsedTokensRequested(this.customerId);
    }
    @Then("a CustomerUsedTokensFoundFailed event is sent")
    public void a_customer_used_tokens_found_failed_event_is_sent() {
        CustomerUsedTokensFoundFailed customerUsedTokensFoundFailed =
                new CustomerUsedTokensFoundFailed(
                        String.format("Customer with id %s does not have any used tokens", this.customerId.getId()));
        event = new Event(TokenService.CUSTOMER_USED_TOKENS_FOUND_FAILED, new Object[]{ customerUsedTokensFoundFailed, eventId});
        verify(messageQueue).publish(event);
    }
    @Then("the event contains an errormessage saying {string}")
    public void the_event_contains_an_errormessage_saying(String errorMessage) {
        Assert.assertEquals(errorMessage, this.event.getArgument(0, CustomerUsedTokensFoundFailed.class).getErrorMessage());
    }
}
