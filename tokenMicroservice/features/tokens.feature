# @author Rasmus Ulrik Kristensen (s233787)

Feature: Token service

  Scenario: User identification
    Given an existing unused token for a Customer with customerId "013c05be-1349-4528-aa4c-959a8ba09b13"
    When a PaymentCreated event containing that token is received
    Then the token is marked as used
    And a CustomerIdFound event is sent with the corresponding customerId

  Scenario: User identification failed used token
    Given an existing used token for a Customer with customerId "013c05be-1349-4528-aa4c-959a8ba09b13"
    When a PaymentCreated event containing that token is received
    Then a CustomerIdFoundFailed event is sent

  Scenario: User identification failed unknown token
    Given an unknown token for a Customer with customerId "013c05be-1349-4528-aa4c-959a8ba09b13"
    When a PaymentCreated event containing that token is received
    Then a CustomerIdFoundFailed event is sent

  Scenario: Valid token generation
    Given a customer with customerId "c5e6cb24-ca5f-44db-bef1-2a15a7ba77af"
    And the customer has 0 unused token
    When a TokensRequested event is received with amount 2
    Then a TokensCreated event is sent with the tokenIds
    And 2 unused tokens exist for the customer

  Scenario: Invalid token generation too many tokens existing
    Given a customer with customerId "c5e6cb24-ca5f-44db-bef1-2a15a7ba77af"
    And the customer has 2 unused token
    When a TokensRequested event is received with amount 2
    Then a TokensGenerationFailed event is sent with message "Token limit exceeded"
    And 2 unused tokens exist for the customer

  Scenario: Invalid token generation too many tokens requested
    Given a customer with customerId "c5e6cb24-ca5f-44db-bef1-2a15a7ba77af"
    And the customer has 0 unused token
    When a TokensRequested event is received with amount 6
    Then a TokensGenerationFailed event is sent with message "noOfTokens must be between 1 and 5"
    And 0 unused tokens exist for the customer

  Scenario: Invalid token generation too few tokens requested
    Given a customer with customerId "c5e6cb24-ca5f-44db-bef1-2a15a7ba77af"
    And the customer has 0 unused token
    When a TokensRequested event is received with amount -1
    Then a TokensGenerationFailed event is sent with message "noOfTokens must be between 1 and 5"
    And 0 unused tokens exist for the customer

  Scenario: CustomerUsedTokensRequested event received sends CustomerUsedTokensFound
    Given a CustomerUsedTokensRequested event with customerId "c5e6cb24-ca5f-44db-bef1-2a15a7ba77af"
    And the customer has two used tokens
    When a CustomerUsedTokensRequested event is received
    Then a CustomerUsedTokensFound event is sent
    And the event contains the two used tokens

  Scenario: CustomerUsedTokensRequested event received sends CustomerUsedTokensFoundFailed
    Given a CustomerUsedTokensRequested event with customerId "c5e6cb24-ca5f-44db-bef1-2a15a7ba77af" without any used tokens
    When a CustomerUsedTokensRequested event is received
    Then a CustomerUsedTokensFoundFailed event is sent
    And the event contains an errormessage saying "Customer with id c5e6cb24-ca5f-44db-bef1-2a15a7ba77af does not have any used tokens"
