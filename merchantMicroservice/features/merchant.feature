# @author Jonas Løvenhardt Henriksen (s195457)

Feature: Merchant

  Scenario: PaymentCreated event received sends MerchantBankAccountIdFound event
    Given an existing merchant with merchantId "8e257cf7-15e8-4b45-b7f2-40e84dd3a738"
    And bankAccountId "d4bc245b-0ac9-4a12-a551-888f63fe7244"
    When a "PaymentCreated" event is received
    Then the "MerchantBankAccountIdFound" event is created with the bankAccountId
    And the event is sent

  Scenario: MerchantAccountRequested event received sends MerchantAccountCreated event
    Given a MerchantAccountRequested event is received
    Then a MerchantAccountCreated event is sent
    And the merchant gets a merchant id

  Scenario: Fetch newly created merchant from repository
    Given bankAccountId "d4bc245b-0ac9-4a12-a551-888f63fe7244"
    When a merchant is created
    Then a merchantId is returned
    And the merchant exists in the repository with the bankAccountId and a merchantId

  Scenario: MerchantAccountDeleteRequested event received sends MerchantAccountDeleted event
    Given an existing merchant
    When a MerchantAccountDeleteRequested event is received
    Then a MerchantAccountDeleted event is sent
    And the merchant does not exist in the repository

  Scenario: MerchantAccountDeleteRequested event received sends MerchantAccountDeleteFailed event
    Given a non existing merchant
    When a MerchantAccountDeleteRequested event is received with the invalid merchant id "8e257cf7-15e8-4b45-b7f2-40e84dd3a738"
    Then a MerchantAccountDeleteFailed event is sent with the error message "Merchant account with id: 8e257cf7-15e8-4b45-b7f2-40e84dd3a738 does not exist"

  Scenario: PaymentCreated event received sends MerchantBankAccountIdFoundFailed event
    Given a PaymentCreated event is received
    Then a MerchantBankAccountIdFoundFailed event is sent
    And the event contains the error message "Merchant not found"