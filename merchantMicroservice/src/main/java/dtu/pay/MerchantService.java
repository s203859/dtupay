package dtu.pay;

import dtu.pay.entities.Merchant;
import dtu.pay.events.*;
import dtu.pay.valueObjects.EventId;
import dtu.pay.valueObjects.MerchantId;
import messaging.Event;
import messaging.MessageQueue;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class MerchantService {
    public static final String PAYMENT_CREATED = "PaymentCreated";
    public static final String MERCHANT_BANK_ACCOUNT_ID_FOUND = "MerchantBankAccountIdFound";
    public static final String MERCHANT_BANK_ACCOUNT_ID_FOUND_FAILED = "MerchantBankAccountIdFoundFailed";
    public static final String MERCHANT_ACCOUNT_REQUESTED = "MerchantAccountRequested";
    public static final String MERCHANT_ACCOUNT_CREATED = "MerchantAccountCreated";
    public static final String MERCHANT_ACCOUNT_DELETE_REQUESTED = "MerchantAccountDeleteRequested";
    public static final String MERCHANT_ACCOUNT_DELETED = "MerchantAccountDeleted";
    public static final String MERCHANT_ACCOUNT_DELETE_FAILED = "MerchantAccountDeleteFailed";
    MessageQueue messageQueue;
    IMerchantRepository repository;

    public MerchantService(MessageQueue messageQueue, IMerchantRepository repository) {
        this.messageQueue = messageQueue;
        this.repository = repository;

        messageQueue.addHandler(PAYMENT_CREATED, this::handlePaymentCreated);
        messageQueue.addHandler(MERCHANT_ACCOUNT_REQUESTED, this::handleMerchantAccountRequested);
        messageQueue.addHandler(MERCHANT_ACCOUNT_DELETE_REQUESTED, this::handleMerchantAccountDeleteRequested);
    }


    public void handlePaymentCreated(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("PaymentCreated received" + ", eventId: " + eventId.getId().toString());
        IPaymentCreated paymentCreated = event.getArgument(0, PaymentCreated.class);
        Merchant merchant = repository.getMerchant(paymentCreated.getMerchantId());

        if (merchant == null) {
            MerchantBankAccountIdFoundFailed merchantBankAccountIdFoundFailed =
                    new MerchantBankAccountIdFoundFailed("Merchant not found");
            Event newEvent = new Event(
                    MERCHANT_BANK_ACCOUNT_ID_FOUND_FAILED,
                    new Object[]{merchantBankAccountIdFoundFailed, eventId});
            messageQueue.publish(newEvent);
        } else {
            MerchantBankAccountIdFound merchantBankAccountIdFound =
                    new MerchantBankAccountIdFound(paymentCreated.getPaymentId(), merchant.getBankAccountId());
            Event newEvent = new Event(MERCHANT_BANK_ACCOUNT_ID_FOUND, new Object[]{merchantBankAccountIdFound, eventId});
            messageQueue.publish(newEvent);
        }
    }

    public void handleMerchantAccountRequested(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("MerchantAccountRequested received" + ", eventId: " + eventId.getId().toString());
        MerchantAccountRequested merchantAccountRequested = event.getArgument(0, MerchantAccountRequested.class);

        MerchantId merchantId = repository.createMerchant(merchantAccountRequested.getBankAccountId());
        MerchantAccountCreated merchantAccountCreated = new MerchantAccountCreated(merchantId);
        Event newEvent = new Event(MERCHANT_ACCOUNT_CREATED, new Object[]{merchantAccountCreated, eventId});
        messageQueue.publish(newEvent);
    }

    public void handleMerchantAccountDeleteRequested(Event event) {
        EventId eventId = event.getArgument(1, EventId.class);
        System.out.println("MerchantAccountDeleteRequested received" + ", eventId: " + eventId.getId().toString());
        MerchantAccountDeleteRequested merchantAccountDeleteRequested = event.getArgument(0, MerchantAccountDeleteRequested.class);


        MerchantId merchantId = merchantAccountDeleteRequested.getMerchantId();
        Boolean success = repository.deleteMerchant(merchantId);

        if (success) {
            MerchantAccountDeleted merchantAccountDeleted = new MerchantAccountDeleted();
            Event newEvent = new Event(MERCHANT_ACCOUNT_DELETED, new Object[]{merchantAccountDeleted, eventId});
            messageQueue.publish(newEvent);
        } else {
            String errorMessage = String.format("Merchant account with id: %s does not exist", merchantAccountDeleteRequested.getMerchantId()
                    .getId().toString());
            MerchantAccountDeleteFailed merchantAccountDeleteFailed = new MerchantAccountDeleteFailed(errorMessage);
            Event newEvent = new Event(MERCHANT_ACCOUNT_DELETE_FAILED, new Object[]{merchantAccountDeleteFailed, eventId});
            messageQueue.publish(newEvent);
        }
    }
}
