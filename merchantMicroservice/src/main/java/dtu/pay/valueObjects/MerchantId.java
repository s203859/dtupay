package dtu.pay.valueObjects;

import lombok.Value;

import java.util.UUID;

@Value
public class MerchantId {
    UUID id;

    public static MerchantId randomId() {
        return new MerchantId(UUID.randomUUID());
    }
}
