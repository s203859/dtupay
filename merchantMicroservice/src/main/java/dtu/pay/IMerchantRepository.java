package dtu.pay;

import dtu.pay.entities.Merchant;
import dtu.pay.valueObjects.*;

/**
 * @author Tomas Hagenau Andersen (s233489)
 */
public interface IMerchantRepository {
    Merchant getMerchant(MerchantId merchantId);
    MerchantId createMerchant(BankAccountId bankAccountId);

    Boolean deleteMerchant(MerchantId merchantId);
}
