package dtu.pay;

import messaging.implementations.RabbitMqQueue;

/**
 * @author Jonas Attrup Rasmussen (s203859)
 */
public class StartUp {
    public static void main(String[] args) {
        new StartUp().startUp();
    }

    private void startUp() {
        var mq = new RabbitMqQueue("message_queue");
        var repo = new InMemoryMerchantRepository();
        new MerchantService(mq, repo);
    }
}
