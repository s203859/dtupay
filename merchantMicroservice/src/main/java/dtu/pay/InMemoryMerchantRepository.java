package dtu.pay;

import dtu.pay.entities.Merchant;
import dtu.pay.valueObjects.*;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class InMemoryMerchantRepository implements IMerchantRepository {
    ConcurrentMap<MerchantId, Merchant> merchants;

    public InMemoryMerchantRepository() {
        merchants = new ConcurrentHashMap<>();
    }

    @Override
    public Merchant getMerchant(MerchantId merchantId) {
        return merchants.get(merchantId);
    }

    @Override
    public MerchantId createMerchant(BankAccountId bankAccountId) {
        MerchantId merchantId = MerchantId.randomId();
        Merchant merchant = new Merchant(bankAccountId, merchantId);

        merchants.put(merchantId, merchant);
        return merchantId;
    }

    @Override
    public Boolean deleteMerchant(MerchantId merchantId) {
        var key = merchants.remove(merchantId);
        return key != null;
    }
}
