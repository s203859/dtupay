package dtu.pay.entities;

import dtu.pay.valueObjects.*;
import lombok.Data;

@Data
public class Merchant {
    BankAccountId bankAccountId;
    MerchantId merchantId;

    public Merchant(BankAccountId bankAccountId, MerchantId merchantId) {
        this.bankAccountId = bankAccountId;
        this.merchantId = merchantId;
    }
}
