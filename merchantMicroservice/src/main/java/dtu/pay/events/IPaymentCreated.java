package dtu.pay.events;

import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;

public interface IPaymentCreated {
    PaymentId getPaymentId();
    MerchantId getMerchantId();
}
