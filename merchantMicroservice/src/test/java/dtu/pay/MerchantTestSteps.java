package dtu.pay;

import dtu.pay.entities.Merchant;
import dtu.pay.events.*;
import dtu.pay.valueObjects.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.UUID;

import static org.mockito.Mockito.*;

/**
 * @author Emil Arnika Skydsgaard (s193579)
 */
public class MerchantTestSteps {

    private MessageQueue messageQueue;
    private IMerchantRepository mockMerchantRepository, merchantRepository;
    private MerchantService merchantServiceWithMockRepo, merchantService;
    private MerchantId merchantId;
    private PaymentId paymentId;
    private BankAccountId bankAccountId;
    private Event event;
    private EventId eventId;
    private PaymentCreated paymentCreated;

    @Before
    public void before() {
        messageQueue = mock(MessageQueue.class);
        mockMerchantRepository = mock(InMemoryMerchantRepository.class);
        merchantRepository = new InMemoryMerchantRepository();
        merchantServiceWithMockRepo = new MerchantService(messageQueue, mockMerchantRepository);
        merchantService = new MerchantService(messageQueue, merchantRepository);
    }

    @Given("an existing merchant with merchantId {string}")
    public void an_existing_merchant_with_merchant_id(String merchantId) {
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
    }

    @Given("bankAccountId {string}")
    public void bank_account_id(String bankAccountId) {
        this.bankAccountId = new BankAccountId(bankAccountId);
    }

    @When("a {string} event is received")
    public void a_event_is_received(String eventName) {
        this.paymentId = new PaymentId(UUID.randomUUID());
        this.eventId = new EventId(UUID.randomUUID());

        IPaymentCreated paymentCreatedEvent = new PaymentCreated(this.paymentId, null, this.merchantId);
        Event event = new Event(eventName, new Object[]{paymentCreatedEvent, eventId});

        Merchant merchant = new Merchant(this.bankAccountId, this.merchantId);
        when(mockMerchantRepository.getMerchant(this.merchantId)).thenReturn(merchant);

        merchantServiceWithMockRepo.handlePaymentCreated(event);
    }

    @Then("the {string} event is created with the bankAccountId")
    public void the_event_is_created_with_the_bank_account_id(String eventName) {
        MerchantBankAccountIdFound merchantBankAccountIdFound = new MerchantBankAccountIdFound(this.paymentId, this.bankAccountId);
        event = new Event(eventName, new Object[]{merchantBankAccountIdFound, eventId});
        Assert.assertEquals(this.bankAccountId, event.getArgument(0, MerchantBankAccountIdFound.class).getBankAccountId());
    }

    @Then("the event is sent")
    public void the_event_is_sent() {
        verify(messageQueue).publish(event);
    }

    @Given("a MerchantAccountRequested event is received")
    public void a_merchant_account_requested_event_is_received() {
        BankAccountId bankAccountId = new BankAccountId("1234567890");
        this.eventId = new EventId(UUID.randomUUID());

        MerchantAccountRequested merchantAccountRequested = new MerchantAccountRequested(bankAccountId);

        event = new Event(MerchantService.MERCHANT_ACCOUNT_REQUESTED, new Object[]{merchantAccountRequested, eventId});

        this.merchantId = new MerchantId(UUID.randomUUID());
        when(mockMerchantRepository.createMerchant(bankAccountId)).thenReturn(this.merchantId);

        merchantServiceWithMockRepo.handleMerchantAccountRequested(event);
    }

    @Then("a MerchantAccountCreated event is sent")
    public void a_merchant_account_created_event_is_sent() {
        MerchantAccountCreated merchantAccountCreated = new MerchantAccountCreated(this.merchantId);
        event = new Event(MerchantService.MERCHANT_ACCOUNT_CREATED, new Object[]{merchantAccountCreated, eventId});
        verify(messageQueue).publish(event);

    }

    @Then("the merchant gets a merchant id")
    public void the_merchant_gets_a_merchant_id() {
        Assert.assertEquals(this.merchantId, event.getArgument(0, MerchantAccountCreated.class).getMerchantId());
    }

    @When("a merchant is created")
    public void a_merchant_is_created() {
        merchantId = merchantRepository.createMerchant(bankAccountId);
    }

    @Then("a merchantId is returned")
    public void a_merchant_id_is_returned() {
        Assert.assertNotNull(merchantId);
    }

    @Then("the merchant exists in the repository with the bankAccountId and a merchantId")
    public void the_merchant_exists_in_the_repository_with_the_bank_account_id_and_a_merchant_id() {
        Merchant merchant = merchantRepository.getMerchant(merchantId);
        Assert.assertEquals(bankAccountId, merchant.getBankAccountId());
        Assert.assertNotNull(merchant.getMerchantId());
    }

    @Given("an existing merchant")
    public void an_existing_merchant() {
        BankAccountId bankAccountId = new BankAccountId("1234567890");
        this.merchantId = merchantRepository.createMerchant(bankAccountId);
    }

    @When("a MerchantAccountDeleteRequested event is received")
    public void a_merchant_account_delete_requested_event_is_received() {
        MerchantAccountDeleteRequested merchantAccountDeleteRequested = new MerchantAccountDeleteRequested(this.merchantId);
        this.eventId = new EventId(UUID.randomUUID());

        event = new Event(MerchantService.MERCHANT_ACCOUNT_DELETE_REQUESTED, new Object[]{merchantAccountDeleteRequested, eventId});
        merchantService.handleMerchantAccountDeleteRequested(event);
    }

    @Then("a MerchantAccountDeleted event is sent")
    public void a_merchant_account_deleted_event_is_sent() {
        MerchantAccountDeleted merchantAccountDeleted = new MerchantAccountDeleted();
        event = new Event(MerchantService.MERCHANT_ACCOUNT_DELETED, new Object[]{merchantAccountDeleted, eventId});
        verify(messageQueue).publish(event);
    }

    @Then("the merchant does not exist in the repository")
    public void the_merchant_does_not_exist_in_the_repository() {
        Assert.assertNull(merchantRepository.getMerchant(merchantId));
    }

    @Given("a non existing merchant")
    public void a_non_existing_merchant() {
    }
    @When("a MerchantAccountDeleteRequested event is received with the invalid merchant id {string}")
    public void a_merchant_account_delete_requested_event_is_received_with_the_invalid_merchant_id(String id) {
        this.merchantId = new MerchantId(UUID.fromString(id));
        MerchantAccountDeleteRequested merchantAccountDeleteRequested = new MerchantAccountDeleteRequested(this.merchantId);
        this.eventId = new EventId(UUID.randomUUID());

        event = new Event(MerchantService.MERCHANT_ACCOUNT_DELETE_REQUESTED, new Object[]{merchantAccountDeleteRequested, eventId});
        merchantService.handleMerchantAccountDeleteRequested(event);
    }
    @Then("a MerchantAccountDeleteFailed event is sent with the error message {string}")
    public void a_merchant_account_delete_failed_event_is_sent_with_the_error_message(String errorMessage) {
        MerchantAccountDeleteFailed merchantAccountDeleteFailed = new MerchantAccountDeleteFailed(errorMessage);
        Event event = new Event(MerchantService.MERCHANT_ACCOUNT_DELETE_FAILED, new Object[]{ merchantAccountDeleteFailed, this.eventId});
        verify(messageQueue).publish(event);
    }

    @Given("a PaymentCreated event is received")
    public void a_payment_created_event_is_received() {
        paymentCreated = new PaymentCreated(
                new PaymentId(UUID.randomUUID()),
                new TokenId(UUID.randomUUID()),
                new MerchantId(UUID.randomUUID())
        );

        this.eventId = new EventId(UUID.randomUUID());
        event = new Event(MerchantService.PAYMENT_CREATED, new Object[]{ paymentCreated, eventId});
        merchantService.handlePaymentCreated(event);
    }
    @Then("a MerchantBankAccountIdFoundFailed event is sent")
    public void a_merchant_bank_account_id_found_failed_event_is_sent() {
        MerchantBankAccountIdFoundFailed merchantBankAccountIdFoundFailed =
                new MerchantBankAccountIdFoundFailed("Merchant not found");
        event = new Event(MerchantService.MERCHANT_BANK_ACCOUNT_ID_FOUND_FAILED,
                new Object[]{merchantBankAccountIdFoundFailed, eventId});
        verify(messageQueue).publish(event);
    }
    @Then("the event contains the error message {string}")
    public void the_event_contains_the_error_message(String errorMessage) {
        MerchantBankAccountIdFoundFailed merchantBankAccountIdFoundFailed =
                event.getArgument(0, MerchantBankAccountIdFoundFailed.class);
        Assert.assertEquals(errorMessage, merchantBankAccountIdFoundFailed.getErrorMessage());
    }
}
