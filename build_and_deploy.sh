#!/bin/bash

set -e

### Stage 0
# Clean up old containers
docker-compose down --remove-orphans

### Stage 1
# Build and install shared messaging utilities
pushd messagingUtilities
mvn clean install -T 1C
popd

### Stage 2
# Build all microservices (.jar files)

# Build customer service
pushd customerMicroservice
mvn clean package -T 1C
popd

# Build merchant service
pushd merchantMicroservice 
mvn clean package -T 1C
popd

# Build token service
pushd tokenMicroservice 
mvn clean package -T 1C
popd

# Build payment service
pushd paymentMicroservice 
mvn clean package -T 1C
popd

# Build payment service
pushd dtuPayMicroservice 
mvn clean package -T 1C
popd

### Stage 3
# Build and run all containers
docker-compose up -d --build

### Stage 4
# Run end-to-end tests
echo "Waiting for 10 seconds"
sleep 10

pushd endToEndTest
mvn clean test
popd
