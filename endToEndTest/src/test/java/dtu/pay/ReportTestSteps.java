package dtu.pay;

import dtu.pay.api.CustomerService;
import dtu.pay.api.ManagerService;
import dtu.pay.api.MerchantService;
import dtu.pay.entities.Payment;
import dtu.pay.valueObjects.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.After;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * @author Jonas Attrup Rasmussen (s203859)
 */
public class ReportTestSteps {
    BigDecimal amount;
    TokenId tokenId;
    MerchantId merchantId;
    CustomerId customerId;
    MerchantService merchantService;
    CustomerService customerService;
    ManagerService managerService;
    BankService bankService;
    PaymentId paymentId;
    boolean success;
    String errorMessage;
    BankAccountId merchantAccountId;
    BankAccountId customerAccountId;
    Collection<PaymentId> allPayments;
    Collection<PaymentId> merchantPayments;
    Collection<PaymentId> customerPayments;
    Collection<PaymentId> returnedPayments;
    @Before
    public void before(){
        bankService = new BankServiceService().getBankServicePort();
        merchantService = new MerchantService();
        customerService = new CustomerService();
        managerService = new ManagerService();
        allPayments = new ArrayList<>();
        merchantPayments = null;
        customerPayments = null;
    }
    @After
    public void after() throws BankServiceException_Exception {
        if (merchantAccountId != null){
            bankService.retireAccount(merchantAccountId.getId());
            merchantAccountId = null;
        }
        if (customerAccountId != null){
            bankService.retireAccount(customerAccountId.getId());
            customerAccountId = null;
        }
    }

    private MerchantId createMerchant(){
        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        BankAccountId merchantAccountId = null;
        try {
            merchantAccountId = new BankAccountId(bankService.createAccountWithBalance(merchant, BigDecimal.valueOf(2000)));
        } catch (BankServiceException_Exception e) {
            throw new RuntimeException(e);
        }
        return merchantService.createMerchant(merchantAccountId);
    }

    private CustomerId createCustomer(){
        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        BankAccountId customerAccountId = null;
        try {
            customerAccountId = new BankAccountId(bankService.createAccountWithBalance(customer, BigDecimal.valueOf(2000)));
        } catch (BankServiceException_Exception e) {
            throw new RuntimeException(e);
        }
        return customerService.createCustomer(customerAccountId);
    }

    private TokenId[] getTokenId(CustomerId customerId, int noOfTOkens){
        try {
            return customerService.requestTokens(customerId, noOfTOkens).toArray(TokenId[]::new);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Given("five existing payments")
    public void five_existing_payments() throws Exception {
        MerchantId merchantId1 = createMerchant();
        MerchantId merchantId2 = createMerchant();
        merchantId = merchantId1;
        CustomerId customerId1 = createCustomer();
        CustomerId customerId2 = createCustomer();
        customerId = customerId1;
        var tokens1 = getTokenId(customerId2, 3);
        var tokens2 = getTokenId(customerId1, 2);
        TokenId tokenId1 = tokens1[0];
        TokenId tokenId2 = tokens1[1];
        TokenId tokenId3 = tokens2[0];
        TokenId tokenId4 = tokens2[1];
        TokenId tokenId5 = tokens1[2];
        BigDecimal value = new BigDecimal(100);
        PaymentId paymentId1 = merchantService.requestPayment(tokenId1, merchantId1, value);
        PaymentId paymentId2 = merchantService.requestPayment(tokenId2, merchantId1, value);
        PaymentId paymentId3 = merchantService.requestPayment(tokenId3, merchantId2, value);
        PaymentId paymentId4 = merchantService.requestPayment(tokenId4, merchantId2, value);
        PaymentId paymentId5 = merchantService.requestPayment(tokenId5, merchantId2, value);
        allPayments.add(paymentId1);
        allPayments.add(paymentId2);
        allPayments.add(paymentId3);
        allPayments.add(paymentId4);
        allPayments.add(paymentId5);
        merchantPayments = new ArrayList<>();
        merchantPayments.add(paymentId1);
        merchantPayments.add(paymentId2);
        customerPayments = new ArrayList<>();
        customerPayments.add(paymentId3);
        customerPayments.add(paymentId4);
    }

    @When("the manager requests a report")
    public void the_manager_requests_a_report() {
        //There might be many existing payments in the repo as other tests also push to it
        //And it is not reset everytime as there is no delete of payments
        returnedPayments = managerService
                .getPayments()
                .stream()
                .map(Payment::getPaymentId)
                .filter(x->this.allPayments.contains(x))
                .toList();
    }

    @Then("all the payments are returned")
    public void all_the_payments_are_returned() {
        assertEquals(this.allPayments.size(), this.returnedPayments.size());
        assertTrue(this.allPayments.containsAll(this.returnedPayments));
        assertTrue(this.returnedPayments.containsAll(this.allPayments));
    }

    @When("the customer requests a report")
    public void the_customer_requests_a_report() {
        try {
            returnedPayments = customerService.getPayments(customerId).stream().map(Payment::getPaymentId).toList();
            success = true;
        } catch (Exception e) {
            success = false;
            errorMessage = e.getMessage();
        }
    }

    @Then("the customer payments are returned")
    public void the_customer_payments_are_returned() {
        assertEquals(this.customerPayments.size(), this.returnedPayments.size());
        assertTrue(this.customerPayments.containsAll(this.returnedPayments));
        assertTrue(this.returnedPayments.containsAll(this.customerPayments));
    }

    @When("the non existing customer requests a report")
    public void the_non_existing_customer_requests_a_report() {
        try {
            returnedPayments = null;
            returnedPayments = customerService.getPayments(new CustomerId(UUID.randomUUID())).stream().map(Payment::getPaymentId).toList();
            success = true;
        } catch (Exception e) {
            success = false;
            errorMessage = e.getMessage();
        }
    }

    @Then("no customer payments are returned")
    public void no_customer_payments_are_returned() {
        assertNull(this.returnedPayments);
    }

    @When("the merchant requests a report")
    public void the_merchant_requests_a_report() {
        try {
            returnedPayments = merchantService.getPayments(merchantId).stream().map(Payment::getPaymentId).toList();
            success = true;
        } catch (Exception e) {
            success = false;
            errorMessage = e.getMessage();
        }
    }

    @Then("the merchant payments are returned")
    public void the_merchant_payments_are_returned() {
        assertEquals(this.merchantPayments.size(), this.returnedPayments.size());
        assertTrue(this.merchantPayments.containsAll(this.returnedPayments));
        assertTrue(this.returnedPayments.containsAll(this.merchantPayments));
    }

    @When("the non existing merchant requests a report")
    public void the_non_existing_merchant_requests_a_report() {
        try {
            returnedPayments = null;
            returnedPayments = merchantService.getPayments(new MerchantId(UUID.randomUUID())).stream().map(Payment::getPaymentId).toList();
            success = true;
        } catch (Exception e) {
            success = false;
            errorMessage = e.getMessage();
        }
    }

    @Then("no merchant payments are returned")
    public void no_merchant_payments_are_returned() {
        assertEquals(0, this.returnedPayments.size());
    }
    @Then("the customer report does not succeed")
    public void the_customer_report_does_not_succeed() {
        assertFalse(success);
    }
}
