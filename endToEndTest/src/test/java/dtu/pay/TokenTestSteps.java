package dtu.pay;

import dtu.pay.api.CustomerService;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.TokenId;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Jonas Løvenhardt Henriksen (s195457)
 */
public class TokenTestSteps {
    private CustomerService customerService;
    private CustomerId customerId;
    private int amount;
    private Collection<TokenId> tokenIds;
    private boolean success;

    @Before
    public void setup() {
        customerService = new CustomerService();
    }

    @Given("a customer")
    public void a_customer() {
        customerId = new CustomerId(UUID.randomUUID());
    }

    @When("the customer requests {int} new tokens")
    public void the_customer_requests_new_tokens(int amount) {
        try {
            this.amount = amount;
            tokenIds = customerService.requestTokens(customerId, amount);
            success = true;
        } catch (Exception e) {
            success = false;
        }
    }

    @Then("new tokens are returned")
    public void new_tokens_are_returned() {
        Assert.assertEquals(amount, tokenIds.size());
        tokenIds.forEach(Assert::assertNotNull);
    }

    @Then("an error is returned")
    public void an_error_is_returned() {
        Assert.assertFalse(success);
    }
}
