package dtu.pay;

import dtu.pay.api.CustomerService;
import dtu.pay.api.ManagerService;
import dtu.pay.api.MerchantService;
import dtu.pay.valueObjects.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.After;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * @author Emil Arnika Skydsgaard (s193579)
 */
public class ConcurrencyTestSteps {
    BankService bankService;
    MerchantService merchantService;
    CustomerService customerService;
    BankAccountId merchantAccountId;
    BankAccountId customerAccountId;
    MerchantId merchantId;
    CustomerId customerId;
    TokenId[] tokens;

    private final CompletableFuture<PaymentId> paymentId1 = new CompletableFuture<>();
    private final CompletableFuture<PaymentId> paymentId2 = new CompletableFuture<>();
    private final CompletableFuture<PaymentId> paymentId3 = new CompletableFuture<>();
    private final CompletableFuture<PaymentId> paymentId4 = new CompletableFuture<>();
    private final CompletableFuture<PaymentId> paymentId5 = new CompletableFuture<>();


    @Before
    public void before() {
        bankService = new BankServiceService().getBankServicePort();
        merchantService = new MerchantService();
        customerService = new CustomerService();
    }


    @Given("an existing customer and an existing merchant")
    public void an_existing_customer_and_an_existing_merchant() throws Exception {
        merchantId = createMerchant();
        customerId = createCustomer();
        tokens = customerService.requestTokens(customerId, 5).toArray(TokenId[]::new);
    }

    @When("five different payments are requested concurrently")
    public void different_payments_are_requested_concurrently() {
        new Thread(() -> {
            try {
                paymentId1.complete(merchantService.requestPayment(tokens[0], merchantId, new BigDecimal(100)));
            } catch (Exception e) {
                paymentId1.complete(null);
            }
        }).start();
        new Thread(() -> {
            try {
                paymentId2.complete(merchantService.requestPayment(tokens[1], merchantId, new BigDecimal(200)));
            } catch (Exception e) {
                paymentId2.complete(null);
            }
        }).start();
        new Thread(() -> {
            try {
                paymentId3.complete(merchantService.requestPayment(tokens[2], merchantId, new BigDecimal(300)));
            } catch (Exception e) {
                paymentId3.complete(null);
            }
        }).start();
        new Thread(() -> {
            try {
                paymentId4.complete(merchantService.requestPayment(tokens[3], merchantId, new BigDecimal(400)));
            } catch (Exception e) {
                paymentId4.complete(null);
            }
        }).start();
        new Thread(() -> {
            try {
                paymentId5.complete(merchantService.requestPayment(tokens[4], merchantId, new BigDecimal(500)));
            } catch (Exception e) {
                paymentId5.complete(null);
            }
        }).start();
    }

    @Then("all payments succeed")
    public void all_payments_succeed() {
        PaymentId paymentId1 = this.paymentId1.join();
        PaymentId paymentId2 = this.paymentId2.join();
        PaymentId paymentId3 = this.paymentId3.join();
        PaymentId paymentId4 = this.paymentId4.join();
        PaymentId paymentId5 = this.paymentId5.join();
        Assert.assertNotNull(paymentId1);
        Assert.assertNotNull(paymentId2);
        Assert.assertNotNull(paymentId3);
        Assert.assertNotNull(paymentId4);
        Assert.assertNotNull(paymentId5);

        Set<PaymentId> set = new HashSet<>();
        set.add(paymentId1);
        set.add(paymentId2);
        set.add(paymentId3);
        set.add(paymentId4);
        set.add(paymentId5);
        Assert.assertEquals(5, set.size());
    }

    @After
    public void after() throws BankServiceException_Exception {
        if (merchantAccountId != null) {
            bankService.retireAccount(merchantAccountId.getId());
            merchantAccountId = null;
        }
        if (customerAccountId != null) {
            bankService.retireAccount(customerAccountId.getId());
            customerAccountId = null;
        }
    }

    private MerchantId createMerchant() {
        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        BankAccountId merchantAccountId = null;
        try {
            merchantAccountId = new BankAccountId(bankService.createAccountWithBalance(merchant, BigDecimal.valueOf(2000)));
        } catch (BankServiceException_Exception e) {
            throw new RuntimeException(e);
        }
        return merchantService.createMerchant(merchantAccountId);
    }

    private CustomerId createCustomer() {
        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        BankAccountId customerAccountId = null;
        try {
            customerAccountId = new BankAccountId(bankService.createAccountWithBalance(customer, BigDecimal.valueOf(2000)));
        } catch (BankServiceException_Exception e) {
            throw new RuntimeException(e);
        }
        return customerService.createCustomer(customerAccountId);
    }
}
