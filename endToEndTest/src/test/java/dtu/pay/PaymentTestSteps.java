package dtu.pay;

import dtu.pay.api.CustomerService;
import dtu.pay.api.MerchantService;
import dtu.pay.valueObjects.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.After;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class PaymentTestSteps {
    BigDecimal amount;
    TokenId tokenId;
    MerchantId merchantId;
    CustomerId customerId;
    MerchantService merchantService;
    CustomerService customerService;
    BankService bankService;
    PaymentId paymentId;
    boolean success;
    String errorMessage;
    BankAccountId merchantAccountId;
    BankAccountId customerAccountId;

    @Before
    public void before(){
        bankService = new BankServiceService().getBankServicePort();
        merchantService = new MerchantService();
        customerService = new CustomerService();
    }
    @After
    public void after() throws BankServiceException_Exception {
        if (merchantAccountId != null){
            bankService.retireAccount(merchantAccountId.getId());
            merchantAccountId = null;
        }
        if (customerAccountId != null){
            bankService.retireAccount(customerAccountId.getId());
            customerAccountId = null;
        }
    }
    @Given("a payment request for amount {int}")
    public void a_payment_request_for_amount(int amount) {
        this.amount = new BigDecimal(amount);
    }
    @Given("non existing tokenId {string}")
    public void non_existing_token_id(String tokenId) {
        this.tokenId = new TokenId(UUID.fromString(tokenId));
    }
    @Given("an existing merchant")
    public void an_existing_merchant() throws BankServiceException_Exception {
        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        merchantAccountId = new BankAccountId(bankService.createAccountWithBalance(merchant, BigDecimal.valueOf(2000)));
        merchantId = merchantService.createMerchant(merchantAccountId);
    }
    @When("a payment request is sent")
    public void a_payment_request_is_sent() {
        try {
            paymentId = merchantService.requestPayment(tokenId, merchantId, amount);
            success = true;
        } catch (Exception e) {
            errorMessage = e.getMessage();
            success = false;
        }
    }
    @Then("the payment is not successful")
    public void the_payment_is_not_successful() {
        Assert.assertFalse(success);
    }
    @Then("an error for payments is thrown with message {string}")
    public void an_error_for_payments_is_thrown_with_message(String errorMessage) {
        assertNotNull(errorMessage);
        assertEquals(errorMessage, this.errorMessage);
    }
    @Given("an existing tokenId")
    public void an_existing_token_id() throws BankServiceException_Exception {
        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        customerAccountId = new BankAccountId(bankService.createAccountWithBalance(customer, BigDecimal.valueOf(2000)));
        customerId = customerService.createCustomer(customerAccountId);
        try {
            var tokens = customerService.requestTokens(customerId, 1);
            this.tokenId = tokens.stream().findFirst().get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Given("a non existing merchant")
    public void a_non_existing_merchant() {
        this.merchantId = new MerchantId(UUID.randomUUID());
    }
    @Then("the payment is successful")
    public void the_payment_is_successful() {
        Assert.assertTrue(success);
    }
    @Given("an existing merchant with non existing bank account id")
    public void an_existing_merchant_with_non_existing_bank_account_id() {
        merchantId = merchantService.createMerchant(new BankAccountId(UUID.randomUUID().toString()));
    }
    @Given("an existing tokenId to a customer with a non existing bank account id")
    public void an_existing_token_id_to_a_customer_with_a_non_existing_bank_account_id() {
        customerId = customerService.createCustomer(new BankAccountId(UUID.randomUUID().toString()));
        try {
            var tokens = customerService.requestTokens(customerId, 1);
            this.tokenId = tokens.stream().findFirst().get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Given("an existing tokenId with a customer with a bank account with {int}")
    public void an_existing_token_id_with_a_customer_with_a_bank_account_with(Integer value) throws BankServiceException_Exception {
        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        customerAccountId = new BankAccountId(bankService.createAccountWithBalance(customer, BigDecimal.valueOf(value)));
        customerId = customerService.createCustomer(customerAccountId);
        try {
            var tokens = customerService.requestTokens(customerId, 1);
            this.tokenId = tokens.stream().findFirst().get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Given("an existing merchant with a bank account with {int}")
    public void an_existing_merchant_with_a_bank_account_with(Integer value) throws BankServiceException_Exception {
        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        merchantAccountId = new BankAccountId(bankService.createAccountWithBalance(merchant, BigDecimal.valueOf(value)));
        merchantId = merchantService.createMerchant(merchantAccountId);
    }
}
