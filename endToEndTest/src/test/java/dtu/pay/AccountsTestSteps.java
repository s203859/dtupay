package dtu.pay;

import dtu.pay.api.CustomerService;
import dtu.pay.api.MerchantService;
import dtu.pay.valueObjects.BankAccountId;
import dtu.pay.valueObjects.CustomerId;
import dtu.pay.valueObjects.MerchantId;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * @author Martin Posselt Munck (s213791)
 */
public class AccountsTestSteps {
    MerchantService merchantService;
    CustomerService customerService;
    MerchantId merchantId;
    CustomerId customerId;
    BankAccountId customerAccountId, merchantAccountId;
    BankService bank;
    boolean success;
    Exception exception;

    @Before
    public void before() {
        merchantService = new MerchantService();
        customerService = new CustomerService();
        bank = new BankServiceService().getBankServicePort();
    }

    @After
    public void clearAccounts() throws Exception {

        if (customerAccountId != null) {
            bank.retireAccount(customerAccountId.getId());
            customerAccountId = null;
        }

        if (merchantAccountId != null) {
            bank.retireAccount(merchantAccountId.getId());
            merchantAccountId = null;
        }
    }

    //region Merchant
    @Given("a merchant with a bank account")
    public void a_merchant_with_a_bank_account() throws Exception {
        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        merchantAccountId = new BankAccountId(bank.createAccountWithBalance(merchant, BigDecimal.valueOf(2000)));
    }

    @When("the merchant creates a DTUPay account")
    public void the_merchant_creates_a_dtu_pay_account() {
        merchantId = merchantService.createMerchant(merchantAccountId);
    }
    @Then("a merchant id is returned")
    public void a_merchant_id_is_returned() {
        assertNotNull(merchantId);
    }

    @Given("an existing merchant DTUPay account")
    public void an_existing_merchant_dtu_pay_account() throws BankServiceException_Exception {
        User merchant = new User();
        merchant.setCprNumber(UUID.randomUUID().toString());
        merchant.setFirstName(UUID.randomUUID().toString());
        merchant.setLastName(UUID.randomUUID().toString());

        merchantAccountId = new BankAccountId(bank.createAccountWithBalance(merchant, BigDecimal.valueOf(2000)));
        merchantId = merchantService.createMerchant(merchantAccountId);
    }
    @When("trying to delete the merchant account")
    public void trying_to_delete_the_merchant_account() {
        try {
            merchantService.deleteMerchant(merchantId);
            success = true;
        } catch (Exception e) {
            success = false;
            exception = e;
        }
    }

    @Given("a non existing merchant DTUPay account with id {string}")
    public void a_non_existing_merchant_dtu_pay_account_with_id(String merchantId) {
        this.merchantId = new MerchantId(UUID.fromString(merchantId));
    }
    //endregion

    //region Customer
    @Given("a customer with a bank account")
    public void a_customer_with_a_bank_account() throws Exception {
        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        customerAccountId = new BankAccountId(bank.createAccountWithBalance(customer, BigDecimal.valueOf(1000)));
    }
    @When("the customer creates a DTUPay account")
    public void the_customer_creates_a_dtu_pay_account() {
        customerId = customerService.createCustomer(customerAccountId);
    }
    @Then("a customer id is returned")
    public void a_customer_id_is_returned() {
        assertNotNull(customerId);
    }

    @Given("an existing customer DTUPay account")
    public void an_existing_customer_dtu_pay_account() throws BankServiceException_Exception {
        User customer = new User();
        customer.setCprNumber(UUID.randomUUID().toString());
        customer.setFirstName(UUID.randomUUID().toString());
        customer.setLastName(UUID.randomUUID().toString());

        customerAccountId = new BankAccountId(bank.createAccountWithBalance(customer, BigDecimal.valueOf(1000)));
        customerId = customerService.createCustomer(customerAccountId);
    }
    @When("trying to delete the customer account")
    public void trying_to_delete_the_customer_account() {
        try {
            customerService.deleteCustomer(customerId);
            success = true;
        } catch (Exception e) {
            success = false;
            exception = e;
        }
    }
    @Then("it succeeds")
    public void it_succeeds() {
        assertTrue(success);
    }

    @Given("a non existing customer DTUPay account with id {string}")
    public void a_non_existing_customer_dtu_pay_account_with_id(String customerId) {
        this.customerId = new CustomerId(UUID.fromString(customerId));
    }
    //endregion

    @Then("an error is thrown with message {string}")
    public void an_error_is_thrown_with_message(String errorMessage) {
        assertNotNull(exception);
        assertEquals(errorMessage, exception.getMessage());
    }
    @Then("it does not succeed")
    public void it_does_not_succeed() {
        assertFalse(success);
    }
}
