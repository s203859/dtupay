package dtu.pay.valueObjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentId {
    UUID id;
}
