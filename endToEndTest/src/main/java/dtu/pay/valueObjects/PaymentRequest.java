package dtu.pay.valueObjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest {
    TokenId tokenId;
    MerchantId merchantId;
    BigDecimal amount;
}
