package dtu.pay.api;

import dtu.pay.entities.Payment;
import dtu.pay.valueObjects.BankAccountId;
import dtu.pay.valueObjects.MerchantId;
import dtu.pay.valueObjects.PaymentId;
import dtu.pay.valueObjects.TokenId;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import org.jboss.resteasy.spi.HttpResponseCodes;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Rasmus Ulrik Kristensen (s233787)
 */
public class ManagerService {
    public Collection<Payment> getPayments() {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("managers/payments")
                    .request()
                    .get()) {
                return response.readEntity(new GenericType<>() {});
            }
        }
    }
}
