package dtu.pay.api;

import dtu.pay.entities.Payment;
import dtu.pay.valueObjects.*;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import org.jboss.resteasy.spi.HttpResponseCodes;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Jonas Attrup Rasmussen (s203859)
 */
public class MerchantService {
    public PaymentId requestPayment(TokenId tokenId, MerchantId merchantId, BigDecimal amount) throws Exception {
        try (Client client = ClientBuilder.newClient()) {
            PaymentRequest paymentRequest = new PaymentRequest(tokenId, merchantId, amount);
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("merchants/payments")
                    .request()
                    .post(Entity.json(paymentRequest))) {
                if (response.getStatus() == HttpResponseCodes.SC_BAD_REQUEST)
                    throw new Exception(response.readEntity(String.class));
                return response.readEntity(PaymentId.class);
            }
        }
    }

    public MerchantId createMerchant(BankAccountId bankAccountId) {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("merchants")
                    .request()
                    .post(Entity.json(bankAccountId.getId()))) {
                return response.readEntity(MerchantId.class);
            }
        }
    }

    public void deleteMerchant(MerchantId merchantId) throws Exception {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("merchants").path(String.valueOf(merchantId.getId()))
                    .request()
                    .delete()) {
                if (response.getStatus() == HttpResponseCodes.SC_NOT_FOUND){
                    throw new Exception(response.readEntity(String.class));
                }
            }
        }
    }

    public Collection<Payment> getPayments(MerchantId merchantId) {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("merchants/payments").path(String.valueOf(merchantId.getId()))
                    .request()
                    .get()) {
                return response.readEntity(new GenericType<>() {});
            }
        }
    }
}
