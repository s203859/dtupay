package dtu.pay.api;

import dtu.pay.entities.Payment;
import dtu.pay.valueObjects.*;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import org.jboss.resteasy.spi.HttpResponseCodes;

import java.util.Collection;

/**
 * @author Tomas Hagenau Andersen (s233489)
 */
public class CustomerService {

    public CustomerId createCustomer(BankAccountId bankAccountId) {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("customers")
                    .request()
                    .post(Entity.json(bankAccountId.getId()))) {
                return response.readEntity(CustomerId.class);
            }
        }
    }

    public void deleteCustomer(CustomerId customerId) throws Exception {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("customers").path(String.valueOf(customerId.getId()))
                    .request()
                    .delete()) {
                if (response.getStatus() == HttpResponseCodes.SC_NOT_FOUND){
                    throw new Exception(response.readEntity(String.class));
                }
            }
        }
    }

    public Collection<Payment> getPayments(CustomerId customerId) throws Exception {
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("customers/payments").path(String.valueOf(customerId.getId()))
                    .request()
                    .get()) {
                if (response.getStatus() == HttpResponseCodes.SC_BAD_REQUEST){
                    throw new Exception(response.readEntity(String.class));
                }
                return response.readEntity(new GenericType<>() {});
            }
        }
    }

    public Collection<TokenId> requestTokens(CustomerId customerId, int noOfTokens) throws Exception {
        TokenRequest tokenRequest = new TokenRequest(customerId, noOfTokens);
        try (Client client = ClientBuilder.newClient()) {
            WebTarget baseUrl = client.target("http://localhost:8080/");
            try (var response = baseUrl.path("customers/tokens")
                    .request()
                    .post(Entity.json(tokenRequest))) {

                if (response.getStatus() == HttpResponseCodes.SC_CREATED){
                    return response.readEntity(new GenericType<>() {});
                }
                throw new Exception(response.readEntity(String.class));
            }
        }
    }
}
