# @author Tomas Hagenau Andersen (s233489)

Feature: Token Request

  Scenario: User requests new tokens
    Given a customer
    When the customer requests 3 new tokens
    Then new tokens are returned

  Scenario: User requests new tokens fails
    Given a customer
    When the customer requests 8 new tokens
    Then an error is returned