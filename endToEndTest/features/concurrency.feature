# @author Jonas Løvenhardt Henriksen (s195457)

Feature: Concurrency

  Scenario: Concurrent payments
    Given an existing customer and an existing merchant
    When five different payments are requested concurrently
    Then all payments succeed