# @author Jonas Attrup Rasmussen (s203859)

Feature: report

  Scenario: Manager requests report returns all payments
    Given five existing payments
    When the manager requests a report
    Then all the payments are returned

  Scenario: Customer requests report returns all payments where the customer was involved
    Given five existing payments
    When the customer requests a report
    Then the customer payments are returned

  Scenario: Customer requests report throws an error
    Given five existing payments
    When the non existing customer requests a report
    Then no customer payments are returned
    And the customer report does not succeed

  Scenario: Merchant requests report returns all payments where the merchant was involved
    Given five existing payments
    When the merchant requests a report
    Then the merchant payments are returned

  Scenario: merchant requests report returns empty collection
    Given five existing payments
    When the non existing merchant requests a report
    Then no merchant payments are returned