# @author Emil Arnika Skydsgaard (s193579)

Feature: Payment Request
  
  Scenario: Unsuccessful payment request token not exist or used
    Given a payment request for amount 50
    And non existing tokenId "1eb139b6-51ca-481a-8aa7-889e9599a9c3"
    And an existing merchant
    When a payment request is sent
    Then the payment is not successful
    And an error for payments is thrown with message "Customer token does not exist or have been used"

  Scenario: Unsuccessful payment request merchant not found
    Given a payment request for amount 50
    And an existing tokenId
    And a non existing merchant
    When a payment request is sent
    Then the payment is not successful
    And an error for payments is thrown with message "Merchant not found"

  Scenario: Successful payment request
    Given a payment request for amount 50
    And an existing tokenId with a customer with a bank account with 200
    And an existing merchant with a bank account with 300
    When a payment request is sent
    Then the payment is successful

  Scenario: Unsuccessful payment request merchant bank account id does not exist
    Given a payment request for amount 50
    And an existing tokenId
    And an existing merchant with non existing bank account id
    When a payment request is sent
    Then the payment is not successful
    And an error for payments is thrown with message "Creditor account does not exist"

  Scenario: Unsuccessful payment request customer bank account id does not exist
    Given a payment request for amount 50
    And an existing tokenId to a customer with a non existing bank account id
    And an existing merchant
    When a payment request is sent
    Then the payment is not successful
    And an error for payments is thrown with message "Debtor account does not exist"

  Scenario: Unsuccessful payment request insufficient funds
    Given a payment request for amount 500
    And an existing tokenId with a customer with a bank account with 200
    And an existing merchant with a bank account with 300
    When a payment request is sent
    Then the payment is not successful
    And an error for payments is thrown with message "Debtor balance will be negative"
