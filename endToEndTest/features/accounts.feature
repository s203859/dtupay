# @author Rasmus Ulrik Kristensen (s233787)

Feature: Account management, end to end

  Scenario: Merchant can create an account
    Given a merchant with a bank account
    When the merchant creates a DTUPay account
    Then a merchant id is returned

  Scenario: Customer can create an account
    Given a customer with a bank account
    When the customer creates a DTUPay account
    Then a customer id is returned

  Scenario: Delete existing customer DTUPay account
    Given an existing customer DTUPay account
    When trying to delete the customer account
    Then it succeeds

  Scenario: Delete non existing customer DTUPay account
    Given a non existing customer DTUPay account with id "018b2f19-e79e-7d6a-a56d-29feb6211b04"
    When trying to delete the customer account
    Then an error is thrown with message "Customer account with id: 018b2f19-e79e-7d6a-a56d-29feb6211b04 does not exist"
    And it does not succeed

  Scenario: Delete existing merchant DTUPay account
    Given an existing merchant DTUPay account
    When trying to delete the merchant account
    Then it succeeds

  Scenario: Delete non existing merchant DTUPay account
    Given a non existing merchant DTUPay account with id "018b2f19-e79e-7d6a-a56d-29feb6211b04"
    When trying to delete the merchant account
    Then an error is thrown with message "Merchant account with id: 018b2f19-e79e-7d6a-a56d-29feb6211b04 does not exist"
    And it does not succeed